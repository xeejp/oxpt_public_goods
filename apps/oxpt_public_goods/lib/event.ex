defmodule Oxpt.PublicGoods.Events do
  @moduledoc """
  Documentation for Oxpt.PublicGoodsEvents

  gameで使うイベントを定義する
  """

  defmodule UpdateStateAll do
    @moduledoc """
    すべてのplayer_socketがsubscribeしておくべきイベント。
    全員に対しあるstateを送りたいときに使う

    """
    defstruct [:game_id, event: "update_state", state: %{}]
  end

  defmodule UpdateState do
    @moduledoc """
    guest_idで指定されたゲストのplayer_socketがsubscribeしておくべきイベント。
    そのゲストに対しあるstateを送りたいときに使う

    """
    defstruct [:guest_id, :game_id, event: "update_state", state: %{}]
  end

  defmodule UpdateStateGroup do
    defstruct [:group_member_id, :game_id, event: "update_state", state: %{}]
  end

  defmodule FetchState do
    @moduledoc """
    stateを管理しているオートマトンがsubscribeしておくべきイベント。
    player_socketがspawnした時にDispatchし、クライアントにstateを送る

    """
    defstruct [:game_id, :guest_id]
  end

  defmodule Read do
    defstruct [:game_id, :guest_id]
  end

  defmodule ChangePage do
    defstruct [:game_id, :request_id, :page]
  end

  defmodule ChangeSetting do
    defstruct [:game_id, :request_id, :payload]
  end

  defmodule FinishInvesting do
    @moduledoc """
    投資額を決めたときに
    周りの投資を待つ．
    """
    defstruct [:game_id, :guest_id, :request_id, :value]
  end

  defmodule MoveFinishPage do
    @moduledoc """
    実験が終わったguestを
    結果待ち待機画面に遷移させる
    """
    defstruct [:game_id, :request_id, :guest_id]
  end

  defmodule MoveNextRound do
    @moduledoc """
    次のラウンドへ進む
    """
    defstruct [:game_id, :request_id, :guest_id]
  end

  defmodule FinishPunishing do
    @moduledoc """
    罰則を決めたときに
    周りの罰則を待つ
    """
    defstruct [:game_id, :guest_id, :request_id, :value]
  end
end
