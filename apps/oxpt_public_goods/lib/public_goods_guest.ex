defmodule Oxpt.PublicGoods.Guest do
  @moduledoc """
  Documentation for Oxpt.PublicGoods.Guest
  """

  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Event, Filter}
  alias Oxpt.JoinGame
  alias Oxpt.Persistence
  alias Oxpt.Game
  alias Oxpt.Player
  alias Oxpt.Lounge.Host.AddedDummyGuest

  alias Oxpt.PublicGoods.{Host, Locales}

  alias Oxpt.PublicGoods.Events.{
    FetchState,
    UpdateState,
    UpdateStateAll,
    UpdateStateGroup,
    Read,
    ChangePage,
    ChangeSetting,
    FinishInvesting,
    MoveFinishPage,
    MoveNextRound,
    FinishPunishing
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../guest") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{room_id: _room_id}, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_public_goods",
      category: "category_game_theory"
    }

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id}) do
    Persistence.Game.setup(id, room_id)

    host_game_id =
      unless Application.get_env(:oxpt, :restoring, false) do
        perform(id, %Start{saga: Host.new(room_id, game_id: id)})
      else
        nil
      end

    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %FetchState{game_id: ^id}
          } ->
            true

          %Event{
            body: %AddedDummyGuest{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangePage{game_id: ^id}
          } ->
            true

          %Event{
            body: %Read{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangeSetting{game_id: ^id}
          } ->
            true

          %Event{
            body: %FinishInvesting{game_id: ^id}
          } ->
            true

          %Event{
            body: %MoveFinishPage{game_id: ^id}
          } ->
            true

          %Event{
            body: %MoveNextRound{game_id: ^id}
          } ->
            true

          %Event{
            body: %FinishPunishing{game_id: ^id}
          } ->
            true

          %Event{
            body: %JoinGame{game_id: ^id}
          } ->
            true
        end)
    })

    initial_state = %{
      room_id: room_id,
      host_game_id: host_game_id,
      host_guest_id: nil,
      guest_game_id: id,
      players: %{},
      groups: %{},
      matched: false,
      locales: Locales.get(),
      page: "instruction",
      dummy_guest_list: [],
      round_num: 1,
      guests_per_group: 4,
      is_punishment: false,
      max_punishment: 3,
      multi_punishment: 3,
      payoff_rate: 0.4,
      initial_endowment: 10,
      player_num: 0,
      finished_num: 0,
      public_results: %{},
      active_player: %{}
    }

    {:loop, initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform(id, %Receive{})

    state = handle_event_body(id, event.body, state)
    {:loop, state}
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id, host: true}, state) do
    perform(id, %Request{
      body: %JoinGame{game_id: state.host_game_id, guest_id: guest_id, host: true}
    })

    %{state | host_guest_id: guest_id}
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id}, state) do
    player = new_player(state, guest_id)
    new_state = put_in(state, [:players, guest_id], player)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %FetchState{guest_id: guest_id}, state) do
    perform(id, %Dispatch{
      body: %UpdateState{
        game_id: id,
        guest_id: guest_id,
        state: state
      }
    })

    state
  end

  defp handle_event_body(id, %AddedDummyGuest{dummy_guest_list: dummy_guest_list}, state) do
    new_state = put_in(state, [:dummy_guest_list], dummy_guest_list)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          dummy_guest_list: get_in(new_state, [:dummy_guest_list])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %Read{guest_id: guest_id}, state) do
    new_state = put_in(state, [:players, guest_id, :read], true)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %ChangePage{request_id: request_id, page: page}, state) do
    new_state =
      if state.page == "instruction" && page == "experiment" do
        # playersとgroupsとmatchedを変更
        reset(state)
      else
        state
      end

    new_state =
      new_state
      |> Map.put(:page, page)
      |> put_in([:player_num], Enum.count(new_state.players))

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          page: get_in(new_state, [:page]),
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups]),
          matched: get_in(new_state, [:matched]),
          player_num: get_in(new_state, [:player_num])
        }
      }
    })

    response(id, request_id)

    new_state
  end

  defp handle_event_body(id, %ChangeSetting{request_id: request_id, payload: payload}, state) do
    new_state =
      state
      |> update_locales(payload["locales_temp"])
      |> reset
      |> put_in([:round_num], payload["round_num"])
      |> put_in([:guests_per_group], payload["guests_per_group"])
      |> put_in([:is_punishment], payload["is_punishment"])
      |> put_in([:max_punishment], payload["max_punishment"])
      |> put_in([:multi_punishment], payload["multi_punishment"])
      |> put_in([:payoff_rate], payload["payoff_rate"])
      |> put_in([:initial_endowment], payload["initial_endowment"])

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: new_state
      }
    })

    response(id, request_id)

    new_state
  end

  defp handle_event_body(
         id,
         %FinishInvesting{request_id: request_id, guest_id: guest_id, value: value},
         state
       ) do
    player = get_in(state, [:players, guest_id])
    group_id = player.group_id

    new_state =
      state
      |> put_in([:players, guest_id, :invested], true)
      |> put_in([:players, guest_id, :invest_temp], value)
      |> update_in([:players, guest_id, :sum_player_invest], fn x -> x + value end)
      |> update_in([:groups, group_id, :invested_num], fn x -> x + 1 end)
      |> update_in([:groups, group_id, :sum_investment], fn x -> x + value end)

    perform(id, %Dispatch{
      body: %UpdateStateGroup{
        game_id: id,
        group_member_id: get_in(new_state, [:groups, group_id, :members]),
        state: %{
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups])
        }
      }
    })

    new_state =
      if new_state.groups[group_id].invested_num === new_state.guests_per_group and
           new_state.is_punishment do
        now_round = get_in(new_state, [:groups, group_id, :now_round])

        member_id = get_in(new_state, [:groups, group_id, :members])

        round_member_invest = %{}

        round_member_invest =
          Enum.reduce(member_id, round_member_invest, fn id, obj ->
            round_invest = get_in(new_state, [:players, id, :invest_temp])
            member_invest = %{invest: round_invest}

            _obj =
              obj
              |> put_in([id], member_invest)
          end)

        new_state =
          new_state
          |> put_in([:groups, group_id, :game_state], "punishing")
          |> update_in([:public_results], fn public_results ->
            Map.merge(public_results, %{
              Integer.to_string(now_round) =>
                Map.merge(
                  get_in(public_results, [Integer.to_string(now_round)]) || %{},
                  %{
                    group_id => round_member_invest
                  }
                )
            })
          end)

        new_state =
          Enum.reduce(member_id, new_state, fn id, new_state ->
            new_state
            |> put_in([:players, id, :status], "punishing")
          end)

        new_state =
          new_state
          |> update_in([:groups, group_id, :group_invests], fn list ->
            [round_member_invest | list] |> Enum.reverse()
          end)

        perform(id, %Dispatch{
          body: %UpdateStateGroup{
            game_id: id,
            group_member_id: get_in(new_state, [:groups, group_id, :members]),
            state: %{
              players: get_in(new_state, [:players]),
              groups: get_in(new_state, [:groups])
            }
          }
        })

        perform(id, %Dispatch{
          body: %UpdateStateAll{
            game_id: id,
            state: %{
              public_results: get_in(new_state, [:public_results])
            }
          }
        })

        new_state
      else
        new_state
      end

    new_state =
      if(
        new_state.groups[group_id].invested_num === new_state.guests_per_group and
          !new_state.is_punishment
      ) do
        sum_invest = get_in(new_state, [:groups, group_id, :sum_investment])
        payoff_rate = get_in(new_state, [:payoff_rate])
        initial_endowment = get_in(new_state, [:initial_endowment])

        new_state =
          new_state
          |> put_in([:groups, group_id, :game_state], "confirm_investing")

        now_round = get_in(new_state, [:groups, group_id, :now_round])

        member_id = get_in(new_state, [:groups, group_id, :members])

        round_member_invest = %{}

        round_member_invest =
          Enum.reduce(member_id, round_member_invest, fn id, obj ->
            round_invest = get_in(new_state, [:players, id, :invest_temp])
            member_invest = %{invest: round_invest}

            _obj =
              obj
              |> put_in([id], member_invest)
          end)

        new_state =
          new_state
          |> update_in([:public_results], fn public_results ->
            Map.merge(public_results, %{
              Integer.to_string(now_round) =>
                Map.merge(
                  get_in(public_results, [Integer.to_string(now_round)]) || %{},
                  %{
                    group_id => round_member_invest
                  }
                )
            })
          end)

        new_state =
          Enum.reduce(member_id, new_state, fn id, new_state ->
            invest_you = get_in(new_state, [:players, id, :invest_temp])
            profit = sum_invest * payoff_rate + (initial_endowment - invest_you)

            new_state
            |> put_in([:players, id, :status], "confirm_investing")
            |> update_in([:players, id, :profit], fn x -> x + profit end)
          end)

        new_state =
          new_state
          |> update_in([:groups, group_id, :group_invests], fn list ->
            [round_member_invest | list] |> Enum.reverse()
          end)

        perform(id, %Dispatch{
          body: %UpdateStateGroup{
            game_id: id,
            group_member_id: get_in(new_state, [:groups, group_id, :members]),
            state: %{
              players: get_in(new_state, [:players]),
              groups: get_in(new_state, [:groups])
            }
          }
        })

        perform(id, %Dispatch{
          body: %UpdateStateAll{
            game_id: id,
            state: %{
              public_results: get_in(new_state, [:public_results])
            }
          }
        })

        new_state
      else
        new_state
      end

    response(id, request_id)

    new_state
  end

  defp handle_event_body(id, %MoveFinishPage{request_id: request_id, guest_id: guest_id}, state) do
    _groud_id = get_in(state, [:players, guest_id, :group_id])
    player = get_in(state, [:players, guest_id])

    new_state =
      state
      |> put_in([:players, guest_id, :status], "finished")
      |> put_in([:active_player, guest_id], player)

    f_num =
      Map.values(new_state.players)
      |> Enum.filter(fn x -> x.status === "finished" end)
      |> length

    new_state =
      new_state
      |> put_in([:finished_num], f_num)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players]),
          finished_num: get_in(new_state, [:finished_num]),
          active_player: get_in(new_state, [:active_player])
        }
      }
    })

    if Enum.all?(
         Map.keys(new_state.players),
         &(get_in(new_state, [:players, &1, :status]) == "finished")
       ) do
      perform(id, %Dispatch{
        body: %ChangePage{
          game_id: id,
          page: "result",
          request_id: request_id
        }
      })
    end

    response(id, request_id)

    new_state
  end

  defp handle_event_body(id, %MoveNextRound{request_id: request_id, guest_id: guest_id}, state) do
    player = get_in(state, [:players, guest_id])
    group_id = player.group_id

    new_state =
      state
      |> put_in([:players, guest_id, :invested], false)
      |> put_in([:players, guest_id, :invest_temp], 0)
      |> put_in([:players, guest_id, :punished], false)
      |> put_in([:players, guest_id, :punish_temp], 0)
      |> put_in([:players, guest_id, :punish_to_member], 0)
      |> update_in([:groups, group_id, :invested_num], fn x -> x - 1 end)

    perform(id, %Dispatch{
      body: %UpdateStateGroup{
        game_id: id,
        group_member_id: get_in(new_state, [:groups, group_id, :members]),
        state: %{
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups])
        }
      }
    })

    new_state =
      if(new_state.groups[group_id].invested_num === 0) do
        new_state =
          new_state
          |> put_in([:groups, group_id, :game_state], "investing")
          |> put_in([:groups, group_id, :sum_investment], 0)
          |> update_in([:groups, group_id, :now_round], fn x -> x + 1 end)
          |> put_in([:groups, group_id, :punished_num], 0)

        member_id = get_in(new_state, [:groups, group_id, :members])

        new_state =
          Enum.reduce(member_id, new_state, fn id, new_state ->
            new_state
            |> put_in([:players, id, :status], "investing")
            |> put_in([:players, id, :punished], false)
            |> update_in([:players, id, :round], fn x -> x + 1 end)
          end)

        perform(id, %Dispatch{
          body: %UpdateStateGroup{
            game_id: id,
            group_member_id: get_in(new_state, [:groups, group_id, :members]),
            state: %{
              players: get_in(new_state, [:players]),
              groups: get_in(new_state, [:groups])
            }
          }
        })

        new_state
      else
        new_state
      end

    response(id, request_id)

    new_state
  end

  defp handle_event_body(
         id,
         %FinishPunishing{request_id: request_id, guest_id: guest_id, value: value},
         state
       ) do
    player = get_in(state, [:players, guest_id])
    group_id = player.group_id

    new_state =
      state
      |> put_in([:players, guest_id, :punished], true)
      |> update_in([:groups, group_id, :punished_num], fn x -> x + 1 end)

    member_id = get_in(new_state, [:groups, group_id, :members])

    new_state =
      Enum.reduce(member_id, new_state, fn id, new_state ->
        punish_for_id = value[id]["punish"]

        _new_state =
          new_state
          |> update_in([:players, id, :punish_temp], fn x -> x + punish_for_id end)
          |> update_in([:players, guest_id, :punish_to_member], fn x -> x + punish_for_id end)
      end)

    perform(id, %Dispatch{
      body: %UpdateStateGroup{
        game_id: id,
        group_member_id: get_in(new_state, [:groups, group_id, :members]),
        state: %{
          players: get_in(new_state, [:players]),
          groups: get_in(new_state, [:groups])
        }
      }
    })

    new_state =
      if new_state.groups[group_id].punished_num === new_state.guests_per_group do
        sum_invest = get_in(new_state, [:groups, group_id, :sum_investment])
        payoff_rate = get_in(new_state, [:payoff_rate])
        initial_endowment = get_in(new_state, [:initial_endowment])

        new_state =
          new_state
          |> put_in([:groups, group_id, :game_state], "confirm_punishing")

        member_id = get_in(new_state, [:groups, group_id, :members])

        new_state =
          Enum.reduce(member_id, new_state, fn id, new_state ->
            invest_you = get_in(new_state, [:players, id, :invest_temp])
            punish_you = get_in(new_state, [:players, id, :punish_temp])
            multi_punish = get_in(new_state, [:multi_punishment])

            profit =
              sum_invest * payoff_rate + (initial_endowment - invest_you) -
                punish_you * multi_punish

            new_state
            |> put_in([:players, id, :status], "confirm_punishing")
            |> update_in([:players, id, :profit], fn x -> x + profit end)
          end)

        perform(id, %Dispatch{
          body: %UpdateStateGroup{
            game_id: id,
            group_member_id: get_in(new_state, [:groups, group_id, :members]),
            state: %{
              players: get_in(new_state, [:players]),
              groups: get_in(new_state, [:groups])
            }
          }
        })

        new_state
      else
        new_state
      end

    response(id, request_id)

    new_state
  end

  defp match_groups(state) do
    _initial_endowment = state.initial_endowment
    %{players: players} = state
    group_size = state.guests_per_group

    groups =
      players
      |> Enum.map(&elem(&1, 0))
      |> Enum.shuffle()
      |> Enum.map_reduce(0, fn p, acc -> {{acc, p}, acc + 1} end)
      |> elem(0)
      |> Enum.group_by(fn {i, _} -> Integer.to_string(div(i, group_size)) end, fn {_, p} -> p end)

    reducer = fn {group_id, ids}, {players, groups} ->
      if length(ids) == group_size do
        players =
          Enum.reduce(ids, players, fn id, players ->
            players
            |> put_in([id, :group_id], group_id)
            |> put_in([id, :profit], 0)
            |> put_in([id, :status], "investing")
            |> put_in([id, :round], 1)
            |> put_in([id, :invested], false)
          end)

        groups =
          groups
          |> Map.put(group_id, new_group(ids))
          |> put_in([group_id, :now_round], 1)

        {players, groups}
      else
        players =
          Enum.reduce(ids, players, fn id, players ->
            players
            |> put_in([id, :group_id], group_id)
            |> put_in([id, :profit], 0)
            |> put_in([id, :status], "finished")
            |> put_in([id, :invested], false)
          end)

        groups =
          groups
          |> Map.put(group_id, new_group(ids))
          |> put_in([group_id, :game_state], "visiting")

        {players, groups}
      end
    end

    {players, groups} = Enum.reduce(groups, {players, %{}}, reducer)

    state
    |> Map.merge(%{
      players: players,
      groups: groups
    })
  end

  def new_group(members) do
    %{
      members: members,
      now_round: 1,
      game_state: "investing",
      group_invests: [],
      invested_num: 0,
      punished_num: 0,
      sum_investment: 0
    }
  end

  defp reset(state) do
    # playersとgroups,matchedを変更
    player_id =
      state.players
      |> Enum.map(&elem(&1, 0))

    new_state =
      Enum.reduce(player_id, state, fn id, state ->
        state
        |> put_in([:players, id, :finished], false)
        |> put_in([:players, id, :invest_temp], 0)
        |> put_in([:players, id, :invested], false)
        |> put_in([:players, id, :sum_player_invest], 0)
        |> put_in([:players, id, :profit], 0)
        |> put_in([:players, id, :punish_temp], 0)
        |> put_in([:players, id, :punished], false)
        |> put_in([:players, id, :punish_to_member], 0)
        |> put_in([:players, id, :joined], true)
      end)

    new_state =
      new_state
      |> put_in([:active_player], %{})
      |> put_in([:public_results], %{})

    new_state = match_groups(new_state)

    new_state
    |> Map.merge(%{
      matched: true
    })
  end

  # defp set_status(state, page) do
  #   new_players = Enum.reduce(Map.keys(state.players), state.players, fn key, acc_players->
  #     _acc_players = acc_players |>
  #       update_in([key], fn player ->
  #         player |> Map.merge(%{
  #           status: case page do
  #             "experiment" -> "experiment"
  #             _ -> nil
  #           end
  #         })
  #       end)
  #   end)
  #
  #   state
  #   |> Map.merge(%{
  #     players: new_players
  #   })
  # end

  defp update_locales(state, locales_temp) do
    # localeを変更
    Enum.reduce(Map.keys(locales_temp), state, fn key, acc_state ->
      lang_temp = locales_temp[key]

      update_in(
        acc_state,
        [:locales, String.to_existing_atom(key), :translations],
        fn translations ->
          Enum.reduce(Map.keys(lang_temp), translations, fn trans_key, acc_trans ->
            put_in(
              acc_trans,
              [:variables, String.to_existing_atom(trans_key)],
              lang_temp[trans_key]
            )
          end)
        end
      )
    end)
  end

  defp new_player(state, guest_id) do
    %{
      guest_id: guest_id,
      profit: nil,
      status: nil,
      read: false,
      finished: false,
      joined: state.page != "experiment" && state.page != "result",
      invest_temp: div(state.initial_endowment, 2),
      invested: false,
      sum_player_invest: 0,
      punish_temp: 0,
      punished: false,
      punish_to_member: 0
    }
  end

  defp response(id, request_id, status \\ :ok, payload \\ %{}) do
    send(elem(request_id, 0), {:response, request_id, payload})
    :ok
  end
end
