defmodule Oxpt.PublicGoods.Locales do
  def get do
    %{
      en: %{
        translations: %{
          variables: %{
            shared_01: "Shared",
            own_01: "Own",
            test_01: "Test",
            round_01_s: "round",
            round_01_m: "rounds",
            point_unit_01_s: "point"
          },
          host: %{
            title_01: "Public Goods",
            stepper: %{
              instruction_page_01: "Instruction",
              experiment_page_01: "Experiment",
              result_page_01: "Result",
              back_01: "BACK",
              next_01: "NEXT"
            },
            setting: %{
              title_01: "Setting",
              send_01: "SEND",
              cancel_01: "CANCEL",
              language: %{
                title_01: "Language settings",
                en_01: "English",
                ja_01: "Japanese"
              },
              game: %{
                title_01: "Game settings",
                header_item_01: "item",
                header_info_01: "information",
                setting_01: "setting",
                round_num_01: "Rounds",
                guests_per_group_01: "Number of guests per group",
                is_punishment_01: "Punishment",
                yes_punishment_01: "Yes",
                no_punishment_01: "No",
                max_punishment_01: "Max points for punishment",
                multi_punishment_01: "Punishment multiplier",
                payoff_rate_01: "Payoff rate of goods",
                initial_endowment: "initial endowment",
                apply_01: "apply",
                edit_01: "edit",
                error_space_01: "[error] please fill in the value",
                error_invalid_01: "[error] The input is invalid"
              }
            },
            guest_table: %{
              id_01: "id",
              state_01: "state",
              status: %{
                instruction_01: "instruction",
                result_01: "result",
                investing_01: "investing",
                confirm_investing_01: "confirm investing",
                punishing_01: "punishing",
                confirm_punishing_01: "confirm punishing",
                finished_01: "finished",
                playing_01: "playing",
                visiting_01: "-"
              }
            }
          },
          guest: %{
            instruction: %{
              instruction_title_01: "Instruction",
              next_01: "next",
              back_01: "back",
              slider_text_01: "Public goods investment",
              invest_choice_01: "グループに{{invest}}ポイントを投資し、自分に{{remain}}ポイント残す選択です。",
              confirm_01: "グループに{{invest_group}}ポイントを投資し、自分に{{invest_me}}ポイント残します。よろしいですか？",
              invest_table: %{
                header_average_01: "あなた以外の人の平均公共財投資額(例)",
                header_invest_01: "あなたの投資額",
                header_all_01: "全員の公共財投資額合計",
                header_profit_01: "あなたの利得",
                value_01: "{{value}}",
                profit_01: "{{invest_all}} × {{payoff_rate}} + {{invest_you}} = {{profit}}"
              },
              instructions_01: [
                "<p>あなたは他のメンバーとともに{{guests_per_group}}人1組のグループに属しています。</p><p>このグループのメンバーは実験を通して変わりません。</p>",
                "<p>グループのメンバーには各ラウンドごとに{{initial_endowment}}ポイントずつ与えられます。</p><p>あなたはこの{{initial_endowment}}ポイントを、私的財と公共財に振り分けて投資してください。</p><p>私的財：投資したポイントがそのまま自分の収益になります。</p><p>公共財：メンバー全員の投資したポイントを合計して、{{payoff_rate}}を乗じたポイントが全員(あなたを含む)の収益になります。</p>",
                "<p>各ラウンドの収益は各ラウンドごとに集計され、次のラウンドには持ち越されません。</p><p>つまり、どのラウンドでも投資できるポイントは{{initial_endowment}}ポイントです。収益は累積され、画面右上に表示されます。</p>",
                "<p>この投資を全部で{{round_num}}ラウンド繰り返します。</p>",
                "<p>以下のスライドバーを動かして、利得計算方法を確認しましょう。</p>",
                "<p>質問がある場合は、いま受け付けます。</p><p>実験中は質問をしたり、他の被験者と話をしてはいけません。</p><p>また、他の被験者の画面を見ないでください。</p>"
              ],
              instructions_punishment_01: [
                "<p>あなたは他のメンバーとともに{{guests_per_group}}人1組のグループに属しています。</p><p>このグループのメンバーは実験を通して変わりません。</p>",
                "<p>グループのメンバーには各ラウンドごとに{{initial_endowment}}ポイントずつ与えられます。</p><p>あなたはこの{{initial_endowment}}ポイントを、私的財と公共財に振り分けて投資してください。</p><p>私的財：投資したポイントがそのまま自分の収益になります。</p><p>公共財：メンバー全員の投資したポイントを合計して、{{payoff_rate}}を乗じたポイントが全員(あなたを含む)の収益になります。</p>",
                "<p>各ラウンドの収益は各ラウンドごとに集計され、次のラウンドには持ち越されません。</p><p>つまり、どのラウンドでも投資できるポイントは{{initial_endowment}}ポイントです。収益は累積され、画面右上に表示されます。</p>",
                "<p>各ラウンドの投資が終わると、ポイントが清算された後に、メンバーの公共財投資額が発表されます。ただし、いくら投資したかは分かりますが、だれが投資したかは分かりません。</p>",
                "<p>あなたは、公共財投資額をみながら、他のメンバーに対して罰則を与えることができます。</p><p>罰則は、あなたが1ポイント失うことで、相手に{{multi_punishment}}ポイントを失わさせることができます。</p><p>あなたや相手が失ったポイントは、ただ失われるだけで、だれのものにもなりません。</p><p>また、相手はだれから罰則を受けたのかは分かりません。</p>",
                "<p>あなたは、最大で現ラウンドの収益か{{max_punishment}}ポイントのどちらか小さい値だけ罰則に使うことができます。</p><p>罰則は全く行わなくても問題ありません。</p><p>また、複数のメンバーに振り分けて罰則を与えることもできます。</p>",
                "<p>この投資を全部で{{round_num}}ラウンド繰り返します。</p>",
                "<p>以下のスライドバーを動かして、利得計算方法を確認しましょう。</p>",
                "<p>質問がある場合は、いま受け付けます。</p><p>実験中は質問をしたり、他の被験者と話をしてはいけません。</p><p>また、他の被験者の画面を見ないでください。</p>"
              ]
            },
            experiment: %{
              profit_01: "your profit: {{profit}} point",
              investing: %{
                next_01: "next",
                now_round_01:
                  "The {{now_round}} round now (there are {{left_round}} rounds left)",
                now_round_last_01: "The {{now_round}} round now (this round is the last)",
                instruction_01: "Please determine the amount of investment in group",
                wait_other_01: "Please wait until other members invest",
                invested_01: "invested",
                uninvested_01: "thinking",
                invest_01: "invest",
                cancel_01: "cancel",
                invest_me_01: "Investment in oneself: {{investMe}} point",
                invest_group_01: "Investment in our group: {{investGroup}} point",
                confirm_01: "グループに{{invest_group}}ポイントを投資し、自分に{{invest_me}}ポイント残します。よろしいですか？"
              },
              confirm_investing: %{
                text_01:
                  "<p>グループのメンバーは合計{{sum_investment}}ポイントを公共財に投資しました(あなたを含む)。</p><p>このラウンドのあなたの利得合計は{{round_profit}}ポイントで決定しました。</p><p>{{sum_investment}} × {{payoff_rate}} + {{invest_you}}ポイント = {{round_profit}}ポイント</p>",
                left_round_01: "<p>次のラウンドへ進んでください。</p><p>(残り{{left_round}}ラウンドあります)</p>",
                last_round_01: "<p>次のラウンドへ進んでください。</p><p>(次が最終ラウンドです)</p>",
                finish_01: "<p>すべてのラウンドが終了しました。お疲れさまでした。下のボタンを押して実験を終了してください。</p>",
                notice_01: "全員の投資が終了しました。結果を確認してください。",
                table_head_text_01: "investment",
                next_round_01: "next round",
                finish_experiment_01: "finish"
              },
              punishing: %{
                confirm_01: "以下の表のとおりに罰則を決定します。合計罰則ポイントは{{sum_punishment_you}}ポイントです。よろしいですか？",
                notice_01: "全員の投資が終了しました。結果を確認し、罰則を決めてください。",
                text_01:
                  "<p>グループのメンバーは合計{{sum_investment}}ポイントを公共財に投資しました(あなたを含む)。</p><p>このラウンドのあなたの利得合計は{{round_profit}}ポイントで決定しました。</p><p>{{sum_investment}} × {{payoff_rate}} + {{invest_you}}ポイント = {{round_profit}}ポイント</p><p>罰則額を0 ~ {{max_punishment}} の範囲で決めてください。</p><p>メンバーの表示順はランダムです。前のラウンドの順序から入れ替わっています。</p>",
                punish_result_01:
                  "<p>合計罰則ポイント: {{sum_punishment_you}}ポイント</p><p>罰則適用後のあなたのポイント: {{round_profit}}ポイント - {{sum_punishment_you}}ポイント - ((メンバーからの罰則合計) × {{multi_punishment}})</p>",
                punish_result_error_01:
                  "<p>合計罰則ポイント: {{sum_punishment_you}}ポイント</p><p><font color='#ff0000'>0 ~ {{max_punishment}}の範囲で罰則を決め，正しい値を入力してください。</font></p><p>罰則適用後のあなたのポイント: {{round_profit}}ポイント - {{sum_punishment_you}}ポイント - ((メンバーからの罰則合計) × {{multi_punishment}})</p>",
                wait_01: "グループのメンバー全員が罰則を決定するまでお待ち下さい",
                decide_punishment_01: "罰則を決定する",
                cancel_01: "cancel",
                decide_01: "decide",
                punished_01: "determined",
                unpunished_01: "thinking",
                error_invalid_01: "[error] The input is invalid",
                punish_table: %{
                  header_member_invest_01: "メンバーの投資額",
                  header_punishment_01: "あなたの罰則",
                  header_decrease_point_01: "メンバーの減少ポイント"
                }
              },
              confirm_punishing: %{
                text_01:
                  "<p>グループのメンバーは合計{{sum_investment}}ポイントを公共財に投資しました(あなたを含む)。</p><p>あなたへの罰則は{{punishment_you}} × {{multi_punishment}} = {{result_punishment}}ポイントでした。</p><p>このラウンドのあなたの利得合計は{{round_profit}}ポイントで決定しました。</p><p>{{sum_investment}} × {{payoff_rate}} + {{invest_you}} - {{punish_to_member}} - {{punishment_you}} × {{multi_punishment}} = {{round_profit}}ポイント</p>",
                left_round_01: "<p>次のラウンドへ進んでください。</p><p>(残り{{left_round}}ラウンドあります)</p>",
                last_round_01: "<p>次のラウンドへ進んでください。</p><p>(次が最終ラウンドです)</p>",
                finish_01: "<p>すべてのラウンドが終了しました。お疲れさまでした。下のボタンを押して実験を終了してください。</p>",
                notice_01: "全員が罰則を決定しました。結果を確認してください。",
                next_round_01: "次のラウンドへ",
                finish_experiment_01: "実験を終了する"
              },
              error: %{
                cant_join_01: "Could not join the game. Please wait for this game to end."
              },
              finished: %{
                text_01: "すべての参加者が実験を終えるまでお待ち下さい",
                finished_01: "finished",
                unfinished_01: "playing"
              },
              confirmed: %{
                text_01: "グループのメンバー全員が確認するまでお待ち下さい",
                confirmed_01: "確認済み",
                unconfirmed_01: "未確認"
              },
              notice: %{
                confirm_01: "Confirm"
              }
            },
            result: %{
              graph: %{
                title_01: "結果グラフ",
                invest_01: "公共財に投資されたポイント",
                chart_count_01: "回数",
                chart_round_01: "表示ラウンド: {{chart_round}}",
                all_round_01: "全ラウンド合算"
              },
              transition_graph: %{
                title_01: "公共財平均投資額推移",
                round_01: "ラウンド数",
                point_01: "平均ポイント",
                chart_round_01: "ラウンド{{round_num}}"
              },
              rank: %{
                title_01: "順位表",
                rank_01: "順位",
                guestId_01: "プレイヤーID",
                groupId_01: "グループID",
                invest_01: "公共財への投資額",
                profit_01: "総利益"
              }
            }
          }
        }
      },
      ja: %{
        translations: %{
          variables: %{
            shared_01: "シェア",
            own_01: "オウン",
            test_01: "テスト",
            round_01_s: "ラウンド",
            round_01_m: "ラウンド",
            point_unit_01_s: "ポイント"
          },
          host: %{
            title_01: "公共財実験",
            stepper: %{
              instruction_page_01: "説明",
              experiment_page_01: "実験",
              result_page_01: "結果",
              back_01: "戻る",
              next_01: "次へ"
            },
            setting: %{
              title_01: "設定",
              send_01: "送信",
              cancel_01: "キャンセル",
              language: %{
                title_01: "言語設定",
                en_01: "英語",
                ja_01: "日本語"
              },
              game: %{
                title_01: "ゲーム設定",
                header_item_01: "項目",
                header_info_01: "情報",
                setting_01: "設定",
                round_num_01: "ラウンド数",
                guests_per_group_01: "1グループの人数",
                is_punishment_01: "罰則の有無",
                max_punishment_01: "罰則に使えるポイントの最大値",
                multi_punishment_01: "罰則の倍率",
                payoff_rate_01: "公共財ペイオフ率",
                initial_endowment_01: "初期保有額",
                apply_01: "適用",
                edit_01: "編集",
                error_space_01: "値を入力してください",
                error_invalid_01: "不正な入力です"
              }
            },
            guest_table: %{
              id_01: "id",
              role_01: "役割",
              state_01: "状態",
              status: %{
                instruction_01: "説明",
                result_01: "結果",
                investing_01: "投資",
                confirm_investing_01: "投資結果",
                punishing_01: "罰則",
                confirm_punishing_01: "罰則結果",
                finished_01: "終了",
                playing_01: "実験中",
                visiting_01: "-"
              }
            }
          },
          guest: %{
            instruction: %{
              instruction_title_01: "ゲームの説明",
              next_01: "次へ",
              back_01: "戻る",
              slider_text_01: "公共財投資額",
              invest_choice_01: "グループに{{invest}}ポイントを投資し、自分に{{remain}}ポイント残す選択です。",
              invest_table: %{
                header_average_01: "あなた以外の人の平均公共財投資額(例)",
                header_invest_01: "あなたの投資額",
                header_all_01: "全員の公共財投資額合計",
                header_profit_01: "あなたの利得",
                value_01: "{{value}}",
                profit_01: "{{invest_all}} × {{payoff_rate}} + {{invest_you}} = {{profit}}"
              },
              instructions_01: [
                "<p>あなたは他のメンバーとともに{{guests_per_group}}人1組のグループに属しています。</p><p>このグループのメンバーは実験を通して変わりません。</p>",
                "<p>グループのメンバーには各ラウンドごとに{{initial_endowment}}ポイントずつ与えられます。</p><p>あなたはこの{{initial_endowment}}ポイントを、私的財と公共財に振り分けて投資してください。</p><p>私的財：投資したポイントがそのまま自分の収益になります。</p><p>公共財：メンバー全員の投資したポイントを合計して、{{payoff_rate}}を乗じたポイントが全員(あなたを含む)の収益になります。</p>",
                "<p>各ラウンドの収益は各ラウンドごとに集計され、次のラウンドには持ち越されません。</p><p>つまり、どのラウンドでも投資できるポイントは{{initial_endowment}}ポイントです。収益は累積され、画面右上に表示されます。</p>",
                "<p>各ラウンドの投資が終わると、ポイントが清算された後に、メンバーの公共財投資額が発表されます。ただし、いくら投資したかは分かりますが、だれが投資したかは分かりません。</p><p>この投資を全部で{{round_num}}ラウンド繰り返します。</p>",
                "<p>以下のスライドバーを動かして、利得計算方法を確認しましょう。</p>",
                "<p>質問がある場合は、いま受け付けます。</p><p>実験中は質問をしたり、他の被験者と話をしてはいけません。</p><p>また、他の被験者の画面を見ないでください。</p>"
              ],
              instructions_punishment_01: [
                "<p>あなたは他のメンバーとともに{{guests_per_group}}人1組のグループに属しています。</p><p>このグループのメンバーは実験を通して変わりません。</p>",
                "<p>グループのメンバーには各ラウンドごとに{{initial_endowment}}ポイントずつ与えられます。</p><p>あなたはこの{{initial_endowment}}ポイントを、私的財と公共財に振り分けて投資してください。</p><p>私的財：投資したポイントがそのまま自分の収益になります。</p><p>公共財：メンバー全員の投資したポイントを合計して、{{payoff_rate}}を乗じたポイントが全員(あなたを含む)の収益になります。</p>",
                "<p>各ラウンドの収益は各ラウンドごとに集計され、次のラウンドには持ち越されません。</p><p>つまり、どのラウンドでも投資できるポイントは{{initial_endowment}}ポイントです。収益は累積され、画面右上に表示されます。</p>",
                "<p>各ラウンドの投資が終わると、ポイントが清算された後に、メンバーの公共財投資額が発表されます。ただし、いくら投資したかは分かりますが、だれが投資したかは分かりません。</p>",
                "<p>あなたは、公共財投資額をみながら、他のメンバーに対して罰則を与えることができます。</p><p>罰則は、あなたが1ポイント失うことで、相手に{{multi_punishment}}ポイントを失わさせることができます。</p><p>あなたや相手が失ったポイントは、ただ失われるだけで、だれのものにもなりません。</p><p>また、相手はだれから罰則を受けたのかは分かりません。</p>",
                "<p>あなたは、最大で現ラウンドの収益か{{max_punishment}}ポイントのどちらか小さい値だけ罰則に使うことができます。</p><p>罰則は全く行わなくても問題ありません。</p><p>また、複数のメンバーに振り分けて罰則を与えることもできます。</p>",
                "<p>この投資を全部で{{round_num}}ラウンド繰り返します。</p>",
                "<p>以下のスライドバーを動かして、利得計算方法を確認しましょう。</p>",
                "<p>質問がある場合は、いま受け付けます。</p><p>実験中は質問をしたり、他の被験者と話をしてはいけません。</p><p>また、他の被験者の画面を見ないでください。</p>"
              ]
            },
            experiment: %{
              profit_01: "総利益: {{profit}} ポイント",
              investing: %{
                next_01: "次へ",
                now_round_01: "現在{{now_round}}ラウンド目です(残り{{left_round}}ラウンドあります)",
                now_round_last_01: "現在{{now_round}}ラウンド目です(このラウンドが最後です)",
                instruction_01: "グループへの投資額を決定してください",
                wait_other_01: "他のメンバーが投資するまでお待ち下さい",
                invested_01: "投資済み",
                uninvested_01: "未投資",
                invest_01: "投資する",
                cancel_01: "キャンセルする",
                invest_me_01: "自分への投資額: {{investMe}} ポイント",
                invest_group_01: "グループへの投資額: {{investGroup}} ポイント",
                confirm_01: "グループに{{invest_group}}ポイントを投資し、自分に{{invest_me}}ポイント残します。よろしいですか？"
              },
              confirm_investing: %{
                text_01:
                  "<p>グループのメンバーは合計{{sum_investment}}ポイントを公共財に投資しました(あなたを含む)。</p><p>このラウンドのあなたの利得合計は{{round_profit}}ポイントで決定しました。</p><p>{{sum_investment}} × {{payoff_rate}} + {{invest_you}}ポイント = {{round_profit}}ポイント</p>",
                left_round_01: "<p>次のラウンドへ進んでください。</p><p>(残り{{left_round}}ラウンドあります)</p>",
                last_round_01: "<p>次のラウンドへ進んでください。</p><p>(次が最終ラウンドです)</p>",
                finish_01: "<p>すべてのラウンドが終了しました。お疲れさまでした。下のボタンを押して実験を終了してください。</p>",
                notice_01: "全員の投資が終了しました。結果を確認してください。",
                table_head_text_01: "投資額",
                next_round_01: "次のラウンドへ",
                finish_experiment_01: "実験を終了する"
              },
              punishing: %{
                confirm_01: "以下の表のとおりに罰則を決定します。合計罰則ポイントは{{sum_punishment_you}}ポイントです。よろしいですか？",
                notice_01: "全員の投資が終了しました。結果を確認し、罰則を決めてください。",
                text_01:
                  "<p>グループのメンバーは合計{{sum_investment}}ポイントを公共財に投資しました(あなたを含む)。</p><p>このラウンドのあなたの利得合計は{{round_profit}}ポイントで決定しました。</p><p>{{sum_investment}} × {{payoff_rate}} + {{invest_you}}ポイント = {{round_profit}}ポイント</p><p>罰則額を0 ~ {{max_punishment}} の範囲で決めてください。</p><p>メンバーの表示順はランダムです。前のラウンドの順序から入れ替わっています。</p>",
                punish_result_01:
                  "<p>合計罰則ポイント: {{sum_punishment_you}}ポイント</p><p>罰則適用後のあなたのポイント: {{round_profit}}ポイント - {{sum_punishment_you}}ポイント - ((メンバーからの罰則合計) × {{multi_punishment}})</p>",
                punish_result_error_01:
                  "<p>合計罰則ポイント: {{sum_punishment_you}}ポイント</p><p><font color='#ff0000'>0 ~ {{max_punishment}}の範囲で罰則を決め，正しい値を入力してください。</font></p><p>罰則適用後のあなたのポイント: {{round_profit}}ポイント - {{sum_punishment_you}}ポイント - ((メンバーからの罰則合計) × {{multi_punishment}})</p>",
                wait_01: "グループのメンバー全員が罰則を決定するまでお待ち下さい",
                decide_punishment_01: "罰則を決定する",
                punished_01: "罰を決定済み",
                unpunished_01: "罰を未決定",
                cancel_01: "キャンセル",
                decide_01: "決定",
                error_invalid_01: "不正な入力です",
                punish_table: %{
                  header_member_invest_01: "メンバーの投資額",
                  header_punishment_01: "あなたの罰則",
                  header_decrease_point_01: "メンバーの減少ポイント"
                }
              },
              confirm_punishing: %{
                text_01:
                  "<p>グループのメンバーは合計{{sum_investment}}ポイントを公共財に投資しました(あなたを含む)。</p><p>あなたへの罰則は{{punishment_you}} × {{multi_punishment}} = {{result_punishment}}ポイントでした。</p><p>このラウンドのあなたの利得合計は{{round_profit}}ポイントで決定しました。</p><p>{{sum_investment}} × {{payoff_rate}} + {{invest_you}} - {{punish_to_member}} - {{punishment_you}} × {{multi_punishment}} = {{round_profit}}ポイント</p>",
                left_round_01: "<p>次のラウンドへ進んでください。</p><p>(残り{{left_round}}ラウンドあります)</p>",
                last_round_01: "<p>次のラウンドへ進んでください。</p><p>(次が最終ラウンドです)</p>",
                finish_01: "<p>すべてのラウンドが終了しました。お疲れさまでした。下のボタンを押して実験を終了してください。</p>",
                notice_01: "全員が罰則を決定しました。結果を確認してください。",
                next_round_01: "次のラウンドへ",
                finish_experiment_01: "実験を終了する"
              },
              error: %{
                cant_join_01: "参加できませんでした。ゲームの終了をお待ち下さい。"
              },
              finished: %{
                text_01: "すべての参加者が実験を終えるまでお待ち下さい",
                finished_01: "終了",
                unfinished_01: "プレイ中"
              },
              confirmed: %{
                text_01: "グループのメンバー全員が確認するまでお待ち下さい",
                confirmed_01: "確認済み",
                unconfirmed_01: "未確認"
              },
              notice: %{
                confirm_01: "確認する"
              }
            },
            result: %{
              graph: %{
                title_01: "結果グラフ",
                invest_01: "公共財に投資されたポイント",
                chart_count_01: "回数",
                chart_round_01: "表示ラウンド: {{chart_round}}",
                all_round_01: "全ラウンド合算"
              },
              transition_graph: %{
                title_01: "公共財平均投資額推移",
                round_01: "ラウンド",
                point_01: "平均ポイント",
                chart_group_01: "グループ{{group_num}}",
                all_group_01: "全グループ合算"
              },
              rank: %{
                title_01: "順位表",
                rank_01: "順位",
                guestId_01: "プレイヤーID",
                groupId_01: "グループID",
                invest_01: "公共財への投資額",
                profit_01: "総利益"
              }
            }
          }
        }
      }
    }
  end
end
