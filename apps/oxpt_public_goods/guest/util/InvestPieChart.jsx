import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import i18nInstance from '../i18n'

const InvestPieChart = ({ investMe, investGroup }) => {
  const { locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  return (
    <HighchartsReact
      options={{
        title: {
          text: ''
        },
        tooltip: {
          enabled: false
        },
        plotOptions: {
          pie: {
            size: '100%',
            dataLabels: {
              enabled: true,
              crop: false,
              distance: -50,
            }
          }
        },
        series: [
          {
            type: 'pie',
            data: [
              {
                name: t('guest.experiment.investing.invest_me_01', { investMe: investMe }),
                y: investMe,
                color: '#f7a35c'
              },
              {
                name: t('guest.experiment.investing.invest_group_01', { investGroup: investGroup }),
                y: investGroup,
                color: '#7cb5ec'
              }
            ]

          }
        ],
        credits: {
          enabled: false
        },
        exporting: {
          enabled: false
        }
      }}
      highcharts={Highcharts}
    />
  )
}

InvestPieChart.propTypes = {
  investMe: PropTypes.number,
  investGroup: PropTypes.number
}
export default InvestPieChart
