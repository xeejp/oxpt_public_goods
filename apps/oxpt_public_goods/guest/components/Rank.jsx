import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import SortableTable from './SortableTable'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2)
  },
  rankTable: {
    textAlign: 'center'
  },
  myNumber: {
    backgroundColor: '#FFBABA'
  }
}))

const RankTable = ({ data, index, guestId, classes }) => (
  <Grid item xs={12}>
    <SortableTable data={data} index={index} guestId={guestId} className={classes.rankTable} />
  </Grid>
)

const Rank = () => {
  const classes = useStyles()
  const { locales, guestId, activePlayer } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  const finishedPlayer = Object.keys(activePlayer)

  let data = finishedPlayer
    .map(key => ({
      rank: 1,
      guestId: activePlayer[key].guestId,
      groupId: activePlayer[key].groupId,
      invest: activePlayer[key].sumPlayerInvest,
      profit: activePlayer[key].profit,
    }))
    .sort((a, b) => {
      if (a.profit < b.profit) return 1
      if (a.profit > b.profit) return -1
      return 1
    })

  let f = true; let index
  if (guestId !== null && f && guestId === data[0].key) {
    f = false
    index = 0
  }

  for (let i = 1; i < data.length; i++) {
    if (data[i - 1].profit === data[i].profit) data[i].rank = data[i - 1].rank
    else data[i].rank = i + 1

    if (guestId !== null && f && guestId === data[i].key) {
      f = false
      index = i
    }
  }
  return (
    <RankTable
      className={classes.rankTable}
      data={data}
      index={index}
      guestId={guestId}
      classes={classes}
      t={t}
    />
  )
}

RankTable.propTypes = {
  data: PropTypes.array,
  index: PropTypes.number,
  guestId: PropTypes.string,
  classes: PropTypes.object
}

export default Rank
