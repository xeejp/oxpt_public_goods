import React, { useReducer, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import HCExporting from 'highcharts/modules/exporting'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Add from '@material-ui/icons/Add'
import Remove from '@material-ui/icons/Remove'

import IconButton from '@material-ui/core/IconButton'

import i18nInstance from '../i18n'

HCExporting(Highcharts)

const TransitionChart = ({ publicResults }) => {
  const { locales, roundNum, initialEndowment,  players } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const [groupNum, setGroupNum] = useState(-1)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  const variablesObject = () => {
    return (typeof t('variables')) === 'object' ? t('variables') : {}
  }

  const resultGroupFlatten = (group) => {
    let data_average = []
    for(let i = 1; i <= roundNum; i++) {
      data_average.push(Object.keys(publicResults[i][group]).reduce((acc, guestId) => acc + publicResults[i][group][guestId].invest, 0) / Object.keys(publicResults[i][group]).length)
    }
    return data_average
  } 

  const resultFlatten = () => {
    let data_average = []
    for(let i = 1; i <= roundNum; i++) {
      console.log(Object.keys(publicResults[i]).length)
      let temp_ave = 0
      for(let j = 0; j < Object.keys(publicResults[i]).length; j++) {
        temp_ave = temp_ave + Object.keys(publicResults[i][j]).reduce((acc, guestId) => acc + publicResults[i][j][guestId].invest, 0) / Object.keys(publicResults[i][j]).length
      }
      data_average.push(temp_ave / Object.keys(publicResults[i]).length)
    }
    return data_average
  }

  const compData = (group) => {
    const result = (group >= 0) ? resultGroupFlatten(group) : resultFlatten()
    return result
  }

  const handleDec = () => {
    if (groupNum >= 0) setGroupNum(groupNum - 1)
  }

  const handleInc = () => {
    console.log(groupNum)
    if (groupNum < Object.keys(publicResults[1]).length - 1) setGroupNum(groupNum + 1)
  }

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <HighchartsReact
            options={{
              chart: {
                animation: false,
                inverted: false
              },
              title: {
                text: t('guest.result.transition_graph.title_01')
              },
              xAxis: {
                title: {
                  text: t('guest.result.transition_graph.round_01')
                },
                min: 0,
                max: roundNum + 2,
                tickInterval: 1,
                reserved: false,
              },
              yAxis: {
                title: {
                  text: t('guest.result.transition_graph.point_01')
                },
                min: 0,
                max: initialEndowment,
              },
              series: [{
                showInLegend: false,
                name: t('guest.result.transition_graph.point_01'),
                data: [null, ...compData(groupNum)]
              }],
              credits: {
                enabled: false
              },
              exporting: {
                buttons: {
                  contextButton: {
                    symbol: 'menu',
                    menuItems: ['downloadPNG']
                  }
                },
                fallbackToExportServer: false
              }
            }}
            highcharts={Highcharts}
          />
          <Grid item xs={12}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="baseline"
            >
              <Grid item xs={3}></Grid>
              <Grid item xs={3}>
                <Typography variant='subtitle1'>{(groupNum >= 0) ? t('guest.result.transition_graph.chart_group_01', { group_num: groupNum }) : t('guest.result.transition_graph.all_group_01')}</Typography>
              </Grid>
              <Grid item xs={3}>
                <IconButton
                  color="primary"
                  aria-label="decrease round"
                  onClick={handleDec}
                  disabled={groupNum <= -1}
                >
                  <Remove />
                </IconButton>
                <IconButton
                  color="secondary"
                  aria-label="increase round"
                  onClick={handleInc}
                  disabled={groupNum >= Object.keys(publicResults[1]).length - 1}
                >
                  <Add />
                </IconButton>
              </Grid>
              <Grid item xs={3}></Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  )
}

TransitionChart.propTypes = {
  publicResults: PropTypes.Object,
}

export default TransitionChart

