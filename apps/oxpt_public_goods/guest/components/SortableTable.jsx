import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { useTranslation } from 'react-i18next'

import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import TableBody from '@material-ui/core/TableBody'
import TableSortLabel from '@material-ui/core/TableSortLabel'

import i18nInstance from '../i18n'

const useStyles = makeStyles({
  root: {
    overflowX: 'auto',
    whiteSpace: 'nowrap'
  },
  table: {
    tableLayout: 'fixed'
  },
  myNumber: {
    backgroundColor: '#FFBABA'
  }
})

const SortableTable = ({ data, guestId }) => {
  const classes = useStyles()
  const columns = Object.keys(data[0])

  const [t] = useTranslation('translations', { i18nInstance })

  const [state, setState] = useState({
    rows: data,
    order: 'desc',
    key: columns[0]
  })

  function handleClickSortColumn(column) {
    const isDesc = column === state.key && state.order === 'desc'
    const nextOrder = isDesc ? 'asc' : 'desc'
    const sortRule = { asc: [1, -1], desc: [-1, 1] }
    const sortedRows = state.rows.slice().sort((a, b) => {
      if (a[column] > b[column]) {
        return sortRule[nextOrder][0]
      } else if (a[column] < b[column]) {
        return sortRule[nextOrder][1]
      } else {
        return 0
      }
    })

    setState({
      rows: sortedRows,
      order: nextOrder,
      key: column
    })
  }

  return (
    <Table size="medium" className={classes.table}>
      <TableHead>
        <TableRow>
          {columns.map((column, colIndex) =>
            column === 'guestId' ? null
              : (
                <TableCell
                  align={isNaN(state.rows[0][column]) ? 'left' : 'right'}
                  key={`table-header-col-${colIndex}`}
                  sortDirection={state.key === column ? state.order : false}
                >
                  <TableSortLabel
                    active={state.key === column}
                    direction={state.order}
                    onClick={() => handleClickSortColumn(column)}
                  >
                    {t('guest.result.rank.' + column + '_01')}
                  </TableSortLabel>
                </TableCell>
              )
          )}
        </TableRow>
      </TableHead>
      <TableBody>
        {state.rows.map((row, rowIndex) =>
          guestId === row.guestId ? (
            <TableRow hover key={`table-row-row-${rowIndex}`} className={classes.myNumber}>
              {Object.keys(row).map((key, colIndex) =>
                key === 'guestId' ? null : (
                  <TableCell
                    align={isNaN(row[key]) ? 'left' : 'right'}
                    key={`table-row-${rowIndex}-col-${colIndex}`}
                  >
                    {key === 'groupId' ? row[key] : null}
                    {key === 'rank' ? row[key] + '(You)' : null}
                    {key === 'invest' ? row[key] : null}
                    {key === 'profit' ? row[key].toFixed(1) : null}
                  </TableCell>
                ))}
            </TableRow>
          ) : (
            <TableRow hover key={`table-row-row-${rowIndex}`}>
              {Object.keys(row).map((key, colIndex) =>
                key === 'guestId' ? null : (
                  <TableCell
                    align={isNaN(row[key]) ? 'left' : 'right'}
                    key={`table-row-${rowIndex}-col-${colIndex}`}
                  >
                    {key === 'groupId' ? row[key] : null}
                    {key === 'rank' ? row[key] : null}
                    {key === 'invest' ? row[key] : null}
                    {key === 'profit' ? row[key].toFixed(1) : null}
                  </TableCell>
                ))}
            </TableRow>
          ))}
      </TableBody>
    </Table>
  )
}

SortableTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  guestId: PropTypes.string
}

export default SortableTable
