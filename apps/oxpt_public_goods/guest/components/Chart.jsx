import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Hidden from '@material-ui/core/Hidden'

import Add from '@material-ui/icons/Add'
import Remove from '@material-ui/icons/Remove'

import IconButton from '@material-ui/core/IconButton'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  chart: {
    maxWidth: '100%'
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, publicResults, initialEndowment } = useStore()
  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const [chartRound, setchartRound] = useState(0)

  const categories = Array(11).fill(0).map((_, i) => initialEndowment * i / 10)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const resultFlatten = () => {
    return Object.keys(publicResults).reduce((accResult, key1) => {
      const result = publicResults[key1]
      return Object.keys(result).reduce((accResult2, key2) => {
        const result2 = publicResults[key1][key2]
        return Object.keys(result2).reduce((accResult3, key3) => ({
          ...accResult3,
          [key1 + '_' + key2 + '_' + key3]: result2[key3]
        }), accResult2)
      }, accResult)
    }, {})
  }

  const resultRoundFlatten = (round) => {
    return Object.keys(publicResults[round]).reduce((accResult, key1) => {
      const result = publicResults[round][key1]
      return Object.keys(result).reduce((accResult2, key2) => ({
        ...accResult2,
        [key1 + '_' + key2]: result[key2]
      }), accResult)
    }, {})
  }

  const compData = (round) => {
    const result = (round >= 1) ? resultRoundFlatten(round) : resultFlatten()
    const values = result ? Object.keys(result).map(id => result[id].invest) : []
    return Array.from(categories).map(x => values.filter(y => ((x <= y) && (y < (x + (initialEndowment / 10))))).length)
  }

  const showCategories = (size) => {
    var arr = categories.concat()
    arr = arr.map(function (val, index, array) { return val + (initialEndowment / 10 - 1) })
    var newArr = categories.map(function (val, index, array) { return val + '<br>~<br>' + arr[index] })
    newArr[10] = String(initialEndowment)
    if (size === 'small') {
      newArr[1] = ''
      newArr[3] = ''
      newArr[5] = ''
      newArr[7] = ''
      newArr[9] = ''
    }
    return newArr
  }

  const handleDec = () => {
    if (chartRound > 0) setchartRound(chartRound - 1)
  }

  const handleInc = () => {
    if (chartRound < Object.keys(publicResults).length) setchartRound(chartRound + 1)
  }

  const variablesObject = () => {
    return typeof (t('variables')) === 'object' ? t('variables') : {}
  }

  const varPointUnit = locales[i18n.language].translations.variables['point_unit_01_s']

  if (!(locales))
    return <></>

  return (
    <div className={classes.chart}>
      <Hidden xsDown>
        <Grid container>
          <Grid item xs={12}>
            <HighchartsReact
              options={{
                chart: {
                  type: 'column'
                },
                title: {
                  text: t('guest.result.graph.invest_01', variablesObject())
                },
                xAxis: {
                  categories: showCategories('large'),
                  crosshair: true,
                  title: {
                    text: varPointUnit
                  },
                  labels: {
                    step: 1
                  },
                },
                yAxis: {
                  allowDecimals: false,
                  min: 0,
                  title: {
                    text: t('guest.result.graph.chart_count_01')
                  },
                  labels: {
                    step: 1
                  }
                },
                tooltip: {
                  headerFormat: `<b>{point.x}${varPointUnit}</b><br/>`,
                  pointFormat: `{series.name}:{point.y}<br/>`,
                  shared: true
                },
                series: [
                  {
                    name: t('guest.result.graph.chart_count_01'),
                    data: compData(chartRound),
                    stacking: 'normal'
                  },
                ],
                credits: {
                  enabled: false
                },
                legend: {
                  enabled: false,
                },
              }}
              highcharts={Highcharts}
            />
          </Grid>
          <Grid item xs={12}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="baseline"
            >
              <Grid item xs={3}></Grid>
              <Grid item xs={3}>
                <Typography variant='subtitle1'>{(chartRound >= 1) ? t('guest.result.graph.chart_round_01', { chart_round: chartRound }) : t('guest.result.graph.all_round_01')}</Typography>
              </Grid>
              <Grid item xs={3}>
                <IconButton
                  color="primary"
                  aria-label="decrease round"
                  onClick={handleDec}
                  disabled={chartRound <= 0}
                >
                  <Remove />
                </IconButton>
                <IconButton
                  color="secondary"
                  aria-label="increase round"
                  onClick={handleInc}
                  disabled={chartRound >= Object.keys(publicResults).length}
                >
                  <Add />
                </IconButton>
              </Grid>
              <Grid item xs={3}></Grid>
            </Grid>
          </Grid>
        </Grid>
      </Hidden>
      <Hidden smUp>
        <Grid container>
          <Grid item xs={12}>
            <HighchartsReact
              options={{
                chart: {
                  type: 'column'
                },
                title: {
                  text: t('guest.result.graph.invest_01', variablesObject())
                },
                xAxis: {
                  categories: showCategories('small'),
                  crosshair: true,
                  title: {
                    text: varPointUnit
                  },
                  labels: {
                    step: 1
                  },
                },
                yAxis: {
                  allowDecimals: false,
                  min: 0,
                  title: {
                    text: t('guest.result.graph.chart_count_01')
                  },
                  labels: {
                    step: 1
                  }
                },
                tooltip: {
                  headerFormat: `<b>{point.x}${varPointUnit}</b><br/>`,
                  pointFormat: `{series.name}:{point.y}<br/>`,
                  shared: true
                },
                series: [
                  {
                    name: t('guest.result.graph.chart_count_01'),
                    data: compData(chartRound),
                    stacking: 'normal'
                  },
                ],
                credits: {
                  enabled: false
                },
                legend: {
                  enabled: false
                },
              }}
              highcharts={Highcharts}
            />
          </Grid>
          <Grid item xs={12}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="baseline"
            >
              <Grid item xs={3}></Grid>
              <Grid item xs={3}>
                <Typography variant='subtitle1'>{(chartRound >= 1) ? t('guest.result.graph.chart_round_01', { chart_round: chartRound }) : t('guest.result.graph.all_round_01')}</Typography>
              </Grid>
              <Grid item xs={3}>
                <IconButton
                  color="primary"
                  aria-label="decrease round"
                  onClick={handleDec}
                  disabled={chartRound <= 0}
                >
                  <Remove />
                </IconButton>
                <IconButton
                  color="secondary"
                  aria-label="increase round"
                  onClick={handleInc}
                  disabled={chartRound >= Object.keys(publicResults).length}
                >
                  <Add />
                </IconButton>
              </Grid>
              <Grid item xs={3}></Grid>
            </Grid>
          </Grid>
        </Grid>
      </Hidden>
    </div>
  )
}
