import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import i18nInstance from '../i18n'

const PieChart = ({ mode }) => {
  const { locales, group, guestsPerGroup, playerNum, finishedNum } = useStore()
  const { investedNum, punishedNum } = group

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const getName = (mode) => {
    switch (mode) {
      case 'investing':
        return [t('guest.experiment.investing.uninvested_01'), t('guest.experiment.investing.invested_01')]
      case 'punishing':
        return [t('guest.experiment.punishing.unpunished_01'), t('guest.experiment.punishing.punished_01')]
      case 'finished':
        return [t('guest.experiment.finished.unfinished_01'), t('guest.experiment.finished.finished_01')]
      case 'confirmed':
        return [t('guest.experiment.confirmed.unconfirmed_01'), t('guest.experiment.confirmed.confirmed_01')]
    }
  }

  const name = getName(mode)

  let plotOptions = {
    dataLabels: {
      enabled: true,
      format: '<b>{point.name}</b>: {point.y}'
    }
  }

  let data = []
  const confirmedNum = guestsPerGroup - investedNum

  Object.assign(plotOptions, {
    startAngle: -90,
    endAngle: 90,
    center: ['50%', '75%']
  })

  switch (mode) {
    case 'investing':
      const uninvestedNum = guestsPerGroup - investedNum
      data = [
        {
          name: name[1],
          y: investedNum,
          color: '#7cb5ec'
        },
        {
          name: name[0],
          y: uninvestedNum,
          color: '#f7a35c'
        }
      ]
      break
    case 'punishing':
      const unpunishedNum = guestsPerGroup - punishedNum
      data = [
        {
          name: name[1],
          y: punishedNum,
          color: '#7cb5ec'
        },
        {
          name: name[0],
          y: unpunishedNum,
          color: '#f7a35c'
        }
      ]
      break
    case 'finished':
      const unfinishedNum = playerNum - finishedNum

      data = [
        {
          name: name[1],
          y: finishedNum,
          color: '#7cb5ec'
        },
        {
          name: name[0],
          y: unfinishedNum,
          color: '#f7a35c'
        }
      ]
      break
    case 'confirmed':
      const unconfirmedNum = guestsPerGroup - confirmedNum

      data = [
        {
          name: name[1],
          y: confirmedNum,
          color: '#7cb5ec'
        },
        {
          name: name[0],
          y: unconfirmedNum,
          color: '#f7a35c'
        }
      ]
      break
  }

  if (!(locales && playerNum !== undefined && finishedNum !== undefined))
    return <></>

  return (
    <HighchartsReact
      options={{
        title: {
          text: ''
        },
        tooltip: {
          pointFormat: '<b>{point.y}</b>'
        },
        plotOptions: {
          pie: plotOptions
        },
        series: [
          {
            type: 'pie',
            innerSize: '50%',
            data: data
          }
        ],
        credits: {
          enabled: false
        },
        exporting: {
          enabled: false
        }
      }}
      highcharts={Highcharts}
    />
  )
}

PieChart.propTypes = {
  mode: PropTypes.string
}

export default PieChart
