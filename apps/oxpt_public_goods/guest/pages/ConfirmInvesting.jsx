import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import styled, { keyframes } from 'styled-components'
import { bounce } from 'react-animations'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import CircularProgress from '@material-ui/core/CircularProgress'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import PieChart from '../components/PieChart'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  sliderWrapper: {
    marginTop: '45.25px'
  },
  outer: {
    padding: theme.spacing(1)
  },
  dialogContent: {
    padding: theme.spacing(1),
    align: 'justify'
  },
  button: {
    padding: theme.spacing(2)
  },
  buttonGrid1: {
    paddingRight: theme.spacing(1),
  },
  buttonGrid2: {
    paddingLeft: theme.spacing(1),
  },
  myId: {
    backgroundColor: '#FFBABA'
  }
}))

const Bounce = styled.div`animation: 1.5s ${keyframes`${bounce}`} infinite`

export default () => {
  const classes = useStyles()
  const { locales, group, guestId, invested, investTemp, roundNum, payoffRate, initialEndowment, profit, requestState } = useStore()

  const { groupInvests, sumInvestment, nowRound } = group
  const investYou = initialEndowment - investTemp
  const roundProfit = (sumInvestment * payoffRate + investYou)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)
  const [wait, setWait] = useState(false)

  const [isShuffle, setIsShuffle] = useState(false)
  const [playerKeysShuffle, setPlayerKeysShuffle] = useState([])

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  const shuffleArray = (array) => {
    var tempArray = array
    for (var i = tempArray.length - 1; i > 0; i--) {
      var r = Math.floor(Math.random() * (i + 1))
      var tmp = tempArray[i]
      tempArray[i] = tempArray[r]
      tempArray[r] = tmp
    }
    setPlayerKeysShuffle(tempArray)
    setIsShuffle(true)
    return tempArray
  }
  let memberArray = groupInvests[nowRound - 1]
  const playerKeys = Object.keys(memberArray)
  if (!isShuffle) setPlayerKeysShuffle(isShuffle ? playerKeysShuffle : shuffleArray(playerKeys))

  const handleClick = () => {
    setWait(true)
    if (nowRound === roundNum) {
      requestState({ event: 'move finish page', successCallback: callbackSend, timeoutCallback: callbackSend })
    }
    if (nowRound < roundNum) {
      requestState({ event: 'move next round', successCallback: callbackSend, timeoutCallback: callbackSend })
    }
  }

  const callbackSend = () => {
    setWait(false)
  }

  return (
    <>
      {invested
        ? <Card className={classes.outer + ' animated zoomIn'}>
          <CardContent>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Typography variant='body1' className={classes.profit}>{t('guest.experiment.profit_01', { profit: profit.toFixed(1) })}</Typography>
            </Grid>
            <TableContainer component={Paper} className={classes.table}>
              <Table aria-label="investment table" size="small">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">{t('guest.experiment.confirm_investing.table_head_text_01')}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {playerKeysShuffle.map((key, index) =>
                    guestId === key ? (
                      <TableRow key={ key + '_0_' + index } className={classes.myId}>
                        <TableCell key={ key + '_1_' + index } component="th" scope="row" align="left">
                          {memberArray[key].invest + '(You)'}
                        </TableCell>
                      </TableRow>
                    ) : (
                      <TableRow key={ key + '_0_' + index }>
                        <TableCell key={ key + '_1_' + index } component="th" scope="row" align="left">
                          {memberArray[key].invest}
                        </TableCell>
                      </TableRow>
                    )
                  )}
                </TableBody>
              </Table>
            </TableContainer>
            <Typography
              variant='body1'
              align='justify'
              dangerouslySetInnerHTML={{ __html: t('guest.experiment.confirm_investing.text_01', { sum_investment: sumInvestment, round_profit: roundProfit.toFixed(1), payoff_rate: payoffRate, invest_you: investYou }) }}
            />
            {(roundNum - nowRound) >= 2
              ? <Typography
                variant='body1'
                align='justify'
                dangerouslySetInnerHTML={{ __html: t('guest.experiment.confirm_investing.left_round_01', { left_round: roundNum - nowRound }) }}
              />
              : null}
            {(roundNum - nowRound) === 1
              ? <Typography
                variant='body1'
                align='justify'
                dangerouslySetInnerHTML={{ __html: t('guest.experiment.confirm_investing.last_round_01') }}
              />
              : null}
            {(roundNum - nowRound) === 0
              ? <Typography
                variant='body1'
                align='justify'
                dangerouslySetInnerHTML={{ __html: t('guest.experiment.confirm_investing.finish_01') }}
              />
              : null}
          </CardContent>
          <CardActions>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Grid item xs={12} sm={4}>
                <Bounce>
                  <Button
                    autoFocus={true}
                    color='primary'
                    variant='contained'
                    onClick={handleClick}
                    className={classes.button}
                    fullWidth
                  >
                    {nowRound === roundNum ? wait ? <CircularProgress size={24} className={classes.buttonProgress}/> : t('guest.experiment.confirm_investing.finish_experiment_01') : null}
                    {nowRound < roundNum ? wait ? <CircularProgress size={24} className={classes.buttonProgress}/> : t('guest.experiment.confirm_investing.next_round_01') : null}
                  </Button>
                </Bounce>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
        : <Card className={classes.outer}>
          <CardContent>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Typography variant='body1'>{t('guest.experiment.profit_01', { profit: profit.toFixed(1) })}</Typography>
            </Grid>
            <Typography variant='body1'>{t('guest.experiment.confirmed.text_01')}</Typography>
          </CardContent>
          <CardContent>
            <PieChart mode={'confirmed'} />
          </CardContent>
        </Card>
      }
    </>
  )
}
