import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Investing from './Investing'
import ConfirmInvesting from './ConfirmInvesting'
import ConfirmPunishing from './ConfirmPunishing'
import Punishing from './Punishing'
import Finished from './Finished'
import Result from './Result'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2),
  },
  contents: {
    width: 300,
    margin: 'auto'
  },
  formControl: {
    margin: theme.spacing(3),
  },
  group: {
    margin: theme.spacing(1, 0),
  },
  outer: {
    margin: 0,
    padding: 0
  },
}))

function renderPage(gameState, status) {
  switch (gameState) {
    case 'investing':
      return <Investing />
    case 'confirm_investing':
      if (status === 'finished') {
        return <Finished />
      } else {
        return <ConfirmInvesting />
      }
    case 'confirm_punishing':
      if (status === 'finished') {
        return <Finished />
      } else {
        return <ConfirmPunishing />
      }
    case 'punishing':
      return <Punishing />
    case 'result':
      return <Result />
  }
}

export default () => {
  const classes = useStyles()
  const { locales, status, group } = useStore()
  if (!(locales))
    return <></>

  const { gameState } = group

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (gameState === 'visiting') {
    return (
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="stretch"
        className={classes.outer}
      >
        <Grid item sx={12} sm={10}>
          <Typography variant="body1">{t('guest.experiment.error.cant_join_01')}</Typography>
        </Grid>
      </Grid>
    )
  }

  return (
    <>
      <Grid item xs={12} className={classes.items}>
        {renderPage(gameState, status)}
      </Grid>
    </>
  )
}
