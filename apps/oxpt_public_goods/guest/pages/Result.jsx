import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import clsx from 'clsx'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import Chart from '../components/Chart'
import TransitionChart from '../components/TransitionChart'
import Rank from '../components/Rank'
import i18nInstance from '../i18n'
import grey from '@material-ui/core/colors/grey'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2)
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  inner: {
    margin: theme.spacing(1)
  },
  actionHeader: {
    backgroundColor: grey[200]
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, publicResults } = useStore()

  const [expandGraph, setExpandGraph] = useState(true)
  const [expandTransitionGraph, setExpandTransitionGraph] = useState(false)
  const [expandResult, setExpandResult] = useState(false)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const handleExpandGraph = () => {
    setExpandGraph(!expandGraph)
  }

  const handleExpandTransitionGraph = () => {
    setExpandTransitionGraph(!expandTransitionGraph)
  }

  const handleExpandResult = () => {
    setExpandResult(!expandResult)
  }

  if (!(locales))
    return <></>

  return (
    <>
      <Grid container>
        <Grid item xs={12} className={classes.inner}>
          <Card>
            <CardHeader
              className={classes.actionHeader}
              action={
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expandGraph,
                  })}
                  onClick={handleExpandGraph}
                  aria-expanded={expandGraph}
                  aria-label="expandGraph"
                >
                  <ExpandMoreIcon />
                </IconButton>
              }
              title={t('guest.result.graph.title_01')}
            />
            <Collapse in={expandGraph}>
              <CardContent>
                <Chart />
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
        <Grid item xs={12} className={classes.inner}>
          <Card>
            <CardHeader
              className={classes.actionHeader}
              action={
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expandResult,
                  })}
                  onClick={handleExpandTransitionGraph}
                  aria-expanded={expandTransitionGraph}
                  aria-label="expandTransitionGraph"
                >
                  <ExpandMoreIcon />
                </IconButton>
              }
              title={t('guest.result.transition_graph.title_01')}
            />
            <Collapse in={expandTransitionGraph}>
              <CardContent>
                <TransitionChart 
                  publicResults={publicResults}
                />
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
        <Grid item xs={12} className={classes.inner}>
          <Card>
            <CardHeader
              className={classes.actionHeader}
              action={
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expandResult,
                  })}
                  onClick={handleExpandResult}
                  aria-expanded={expandResult}
                  aria-label="expandResult"
                >
                  <ExpandMoreIcon />
                </IconButton>
              }
              title={t('guest.result.rank.title_01')}
            />
            <Collapse in={expandResult}>
              <CardContent>
                <Rank />
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
      </Grid>
    </>
  )
}
