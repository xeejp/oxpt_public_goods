import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import i18nInstance from '../i18n'

import PieChart from '../components/PieChart'

const useStyles = makeStyles(theme => ({
  outer: {
    padding: theme.spacing(1)
  }
}))

export default () => {
  const classes = useStyles()
  const { locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>
  return (
    <Card className={classes.outer}>
      <CardContent>
        <Typography variant='body1'>{t('guest.experiment.finished.text_01')}</Typography>
      </CardContent>
      <CardContent>
        <PieChart mode={'finished'} />
      </CardContent>
    </Card>
  )
}
