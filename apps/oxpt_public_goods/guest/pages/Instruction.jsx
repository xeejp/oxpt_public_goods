import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import MobileStepper from '@material-ui/core/MobileStepper'
import Button from '@material-ui/core/Button'
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import Slider from '@material-ui/core/Slider'

import SwipeableViews from 'react-swipeable-views'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2),
  },
  instructionItem: {
    padding: theme.spacing(2),
    marginBottom: '12px'
  },
  slider: {
    marginTop: theme.spacing(2) + 'px'
  },
  table: {
    marginBottom: '48px'
  }
}))

export default () => {
  const classes = useStyles()
  const theme = useTheme()
  const { locales, read, pushState, roundNum, guestsPerGroup, isPunishment, maxPunishment, multiPunishment, payoffRate, initialEndowment } = useStore()

  const [activeStep, setActiveStep] = React.useState(0)
  const [value, setValue] = useState(Math.floor(initialEndowment / 2))

  const handleNext = () => {
    if (activeStep === instructionItems().length - 2) pushState({ event: 'read' })
    setActiveStep(prevActiveStep => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1)
  }

  const handleSlideStep = value => {
    if (value === instructionItems().length - 1) pushState({ event: 'read' })
    setActiveStep(value)
  }

  const handleChangeValue = (_, value) => {
    setValue(value)
  }

  const investRateArray = [0, 0.2, 0.4, 0.6, 0.8, 1]

  const instructionItems = () => {
    const variables = { round_num: roundNum, guests_per_group: guestsPerGroup, max_punishment: maxPunishment, multi_punishment: multiPunishment, payoff_rate: payoffRate, initial_endowment: initialEndowment }
    const items = isPunishment ? t('guest.instruction.instructions_punishment_01', variables) : t('guest.instruction.instructions_01', variables)
    return Array.isArray(items) ? items : ['']
  }
  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
    if (!read && instructionItems().length === 1) pushState({ event: 'read' })
  }, [locales])

  if (!(locales))
    return <></>

  const renderInstructionItems = () => {
    return instructionItems().map((text, i) => (
      <Typography
        key={i}
        variant='body1'
        className={classes.instructionItem}
        align='justify'
        dangerouslySetInnerHTML={{ __html: text }}
      />
    ))
  }

  return (
    <>
      <Grid item>
        <Typography variant='subtitle1' className={classes.items}>
          {t('guest.instruction.instruction_title_01')}
        </Typography>
        <SwipeableViews
          index={activeStep}
          onChangeIndex={handleSlideStep.bind(this)}
        >
          {renderInstructionItems()}
        </SwipeableViews>
        {(activeStep === 4 && !isPunishment) || (activeStep === 7 && isPunishment)
          ? <Grid item>
            <p>{t('guest.instruction.slider_text_01')}</p>
            <Slider
              defaultValue={Math.floor(initialEndowment / 2)}
              step={1}
              min={0}
              max={initialEndowment}
              valueLabelDisplay='on'
              onChange={handleChangeValue}
              className={classes.slider}
            />
            <p>{t('guest.instruction.invest_choice_01', { invest: value, remain: initialEndowment - value })}</p>
            <TableContainer component={Paper} className={classes.table}>
              <Table aria-label="setting table" size="small">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">{t('guest.instruction.invest_table.header_average_01')}</TableCell>
                    <TableCell align="center">{t('guest.instruction.invest_table.header_invest_01')}</TableCell>
                    <TableCell align="center">{t('guest.instruction.invest_table.header_all_01')}</TableCell>
                    <TableCell align="center">{t('guest.instruction.invest_table.header_profit_01')}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {investRateArray.map(rate => (
                    <TableRow key={rate}>
                      <TableCell component="th" scope="row" align="left">
                        {t('guest.instruction.invest_table.value_01', { value: Math.floor(initialEndowment * rate) })}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {t('guest.instruction.invest_table.value_01', { value: value })}
                      </TableCell>
                      <TableCell align="center">
                        {t('guest.instruction.invest_table.value_01', { value: value + Math.floor(initialEndowment * rate) * (guestsPerGroup - 1) })}
                      </TableCell>
                      <TableCell align="center">
                        {t('guest.instruction.invest_table.profit_01', { invest_all: value + Math.floor(initialEndowment * rate) * (guestsPerGroup - 1), payoff_rate: payoffRate, invest_you: initialEndowment - value, profit: Math.round(((value + (Math.floor(initialEndowment * rate) * (guestsPerGroup - 1))) * payoffRate + (initialEndowment - value)) * 10) / 10 })}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>

          </Grid>
          : null}
        <MobileStepper
          variant="dots"
          steps={instructionItems().length}
          position="bottom"
          activeStep={activeStep}
          className={classes.root}
          nextButton={
            <Button size="small" onClick={handleNext} disabled={activeStep === instructionItems().length - 1}>
              Next
              {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </Button>
          }
          backButton={
            <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
              {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
              Back
            </Button>
          }
        />
      </Grid>
    </>
  )
}
