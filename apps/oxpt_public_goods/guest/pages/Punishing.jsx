import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import CircularProgress from '@material-ui/core/CircularProgress'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import TextField from '@material-ui/core/TextField'

import PieChart from '../components/PieChart'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  sliderWrapper: {
    marginTop: '45.25px'
  },
  outer: {
    padding: theme.spacing(1)
  },
  dialogContent: {
    padding: theme.spacing(1),
    align: 'justify'
  },
  button: {
    padding: theme.spacing(2)
  },
  buttonGrid1: {
    paddingRight: theme.spacing(1),
  },
  buttonGrid2: {
    paddingLeft: theme.spacing(1),
  },
}))

export default () => {
  const classes = useStyles()
  const { locales, guestId, group, investTemp, payoffRate, maxPunishment, multiPunishment, punished, initialEndowment, profit, requestState } = useStore()

  const { groupInvests, sumInvestment, nowRound } = group
  const investYou = initialEndowment - investTemp
  const roundProfit = (sumInvestment * payoffRate + investYou)

  const [open, setOpen] = useState(false)
  const [sumPunishmentYou, setSumPunishmentYou] = useState(0)
  const [isValid, setIsValid] = useState(true)
  const [isShuffle, setIsShuffle] = useState(false)
  const [playerKeysShuffle, setPlayerKeysShuffle] = useState([])

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)
  const [wait, setWait] = useState(false)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  const shuffleArray = (array) => {
    var tempArray = array
    for (var i = tempArray.length - 1; i > 0; i--) {
      var r = Math.floor(Math.random() * (i + 1))
      var tmp = tempArray[i]
      tempArray[i] = tempArray[r]
      tempArray[r] = tmp
    }
    setPlayerKeysShuffle(tempArray)
    setIsShuffle(true)
    return tempArray
  }

  let memberArray = groupInvests[nowRound - 1]
  const playerKeys = Object.keys(memberArray)
  if (!isShuffle)setPlayerKeysShuffle(isShuffle ? playerKeysShuffle : shuffleArray(playerKeys))
  const initUnits = playerKeys.reduce((units, key) => ({
    ...units,
    [key]: { punish: 0 }
  }), {})
  let returnArray = initUnits
  const [retArray, setRetArray] = useState(returnArray)

  const handleChangePunishment = (key, e) => {
    const temp = e.target.value !== '' ? parseFloat(e.target.value) >= 0 ? parseFloat(e.target.value) : '' : ''
    const tempObject = { ...retArray, [key]: { punish: temp }}
    setRetArray(tempObject)
    let tempSum = 0
    let tempFlag = true
    for (let k of Object.keys(tempObject)) {
      if (tempObject[k].punish !== '') {
        tempSum = tempSum + tempObject[k].punish
      } else {
        tempFlag = false
      }
    }
    setSumPunishmentYou(tempSum)
    if (tempFlag && tempSum <= (roundProfit > maxPunishment ? maxPunishment : roundProfit)) {
      setIsValid(true)
    } else {
      setIsValid(false)
    }
  }

  const handleClick = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleSend = () => {
    setWait(true)
    requestState({ event: 'finish punishing', payload: retArray, successCallback: callbackSend, timeoutCallback: callbackSend })
  }

  const callbackSend = () => {
    setWait(false)
    setOpen(false)
  }

  const handleTextFieldKeyDown = event => {
    if (event.key === 'Enter' || event.keyCode === 13) {
      setOpen(true)
    }
  }

  const handlePunishDialogKeyDown = event => {
    if (event.key === 'Enter' || event.keyCode === 13) {
      handleSend()
    }
  }

  return (
    <>
      {punished
        ? <Card className={classes.outer}>
          <CardContent>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Typography variant='body1'>{t('guest.experiment.profit_01', { profit: profit.toFixed(1) })}</Typography>
            </Grid>
            <Typography variant='body1'>{t('guest.experiment.punishing.wait_01')}</Typography>
          </CardContent>
          <CardContent>
            <PieChart mode={'punishing'} />
          </CardContent>
        </Card>
        : <Card className={classes.outer + ' animated zoomIn'}>
          <CardContent>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Typography variant='body1' className={classes.profit}>{t('guest.experiment.profit_01', { profit: profit.toFixed(1) })}</Typography>
            </Grid>
            <Typography
              variant='body1'
              align='justify'
              dangerouslySetInnerHTML={{ __html: t('guest.experiment.punishing.text_01', { sum_investment: sumInvestment.toFixed(1), round_profit: roundProfit.toFixed(1), payoff_rate: payoffRate, invest_you: investYou, max_punishment: (roundProfit > maxPunishment ? maxPunishment : roundProfit) }) }}
            />
            <TableContainer component={Paper} className={classes.table}>
              <Table aria-label="punishment table" size="small">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">{t('guest.experiment.punishing.punish_table.header_member_invest_01')}</TableCell>
                    <TableCell align="left">{t('guest.experiment.punishing.punish_table.header_punishment_01')}</TableCell>
                    <TableCell align="left">{t('guest.experiment.punishing.punish_table.header_decrease_point_01')}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {playerKeysShuffle.map((key, index) => {
                    if (key === guestId) {
                      return <></>
                    } else {
                      return <TableRow key={key + '_0_' + index}>
                        <TableCell key={key + '_1_' + index} component="th" scope="row" align="left">
                          {memberArray[key].invest}
                        </TableCell>
                        <TableCell key={key + '_2_' + index} component="th" scope="row" align="left">
                          {retArray[key].punish !== ''
                            ? <TextField
                              key={key + '_3_' + index}
                              type='number'
                              step='0.0001'
                              value={retArray[key].punish}
                              onChange={handleChangePunishment.bind(null, key)}
                              onKeyDown={handleTextFieldKeyDown}
                            />
                            : <TextField
                              error
                              key={key + '_4_' + index}
                              type='number'
                              step='0.0001'
                              helperText={t('guest.experiment.punishing.error_invalid_01')}
                              value={retArray[key].punish}
                              onChange={handleChangePunishment.bind(null, key)}
                            />
                          }
                        </TableCell>
                        <TableCell key={key + '_5_' + index} component="th" scope="row" align="left">
                          {retArray[key].punish * multiPunishment}
                        </TableCell>
                      </TableRow>
                    }
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            {isValid
              ? <Typography
                variant='body1'
                align='justify'
                dangerouslySetInnerHTML={{ __html: t('guest.experiment.punishing.punish_result_01', { sum_punishment_you: sumPunishmentYou, round_profit: roundProfit.toFixed(1), multi_punishment: multiPunishment }) }}
              />
              : <Typography
                variant='body1'
                align='justify'
                dangerouslySetInnerHTML={{ __html: t('guest.experiment.punishing.punish_result_error_01', { sum_punishment_you: sumPunishmentYou, round_profit: roundProfit.toFixed(1), multi_punishment: multiPunishment, max_punishment: (roundProfit > maxPunishment ? maxPunishment : roundProfit) }) }}
              />
            }
            <Dialog
              open={open}
              onClose={handleClose}
              onKeyDown={handlePunishDialogKeyDown}
            >
              <DialogContent className={classes.dialogContent}>
                <DialogContentText>
                  {t('guest.experiment.punishing.confirm_01', { sum_punishment_you: sumPunishmentYou })}
                </DialogContentText>
                <TableContainer component={Paper} className={classes.table}>
                  <Table aria-label="punishment table" size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell align="left">{t('guest.experiment.punishing.punish_table.header_member_invest_01')}</TableCell>
                        <TableCell align="left">{t('guest.experiment.punishing.punish_table.header_punishment_01')}</TableCell>
                        <TableCell align="left">{t('guest.experiment.punishing.punish_table.header_decrease_point_01')}</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {playerKeysShuffle.map((key, index) => {
                        if (key === guestId) {
                          return <></>
                        } else {
                          return <TableRow key={key + '_0_' + index}>
                            <TableCell key={key + '_1_' + index} component="th" scope="row" align="left">
                              {memberArray[key].invest}
                            </TableCell>
                            <TableCell key={key + '_2_' + index} component="th" scope="row" align="left">
                              {retArray[key].punish}
                            </TableCell>
                            <TableCell key={key + '_5_' + index} component="th" scope="row" align="left">
                              {retArray[key].punish * multiPunishment}
                            </TableCell>
                          </TableRow>
                        }
                      })}
                    </TableBody>
                  </Table>
                </TableContainer>
              </DialogContent>
              <DialogActions>
                <Grid
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="flex-start"
                >
                  <Grid item xs={6} sm={4} className={classes.buttonGrid1}>
                    <Button variant='outlined' onClick={handleClose} color='secondary' className={classes.button} fullWidth>
                      {t('guest.experiment.punishing.cancel_01')}
                    </Button>
                  </Grid>
                  <Grid item xs={6} sm={4} className={classes.buttonGrid2}>
                    <Button variant='contained' onClick={handleSend} color='primary' className={classes.button} fullWidth>
                      {t('guest.experiment.punishing.decide_01')}
                    </Button>
                  </Grid>
                </Grid>
              </DialogActions>
            </Dialog>
          </CardContent>
          <CardActions>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Grid item xs={12} sm={4}>
                <Button
                  autoFocus={true}
                  color='primary'
                  variant='contained'
                  onClick={handleClick}
                  className={classes.button}
                  disabled={!isValid}
                  fullWidth
                >
                  {wait && <CircularProgress size={24} className={classes.buttonProgress} />}
                  {!wait && t('guest.experiment.punishing.decide_punishment_01')}
                </Button>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
      }
    </>
  )
}
