import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { withStyles, makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import CircularProgress from '@material-ui/core/CircularProgress'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'

import Slider from './Slider'
import InvestPieChart from '../util/InvestPieChart'
import PieChart from '../components/PieChart'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  sliderWrapper: {
    marginTop: '45.25px'
  },
  outer: {
    padding: theme.spacing(1)
  },
  dialogContent: {
    padding: theme.spacing(1),
    align: 'justify'
  },
  button: {
    padding: theme.spacing(2)
  },
  buttonGrid1: {
    paddingRight: theme.spacing(1),
  },
  buttonGrid2: {
    paddingLeft: theme.spacing(1),
  },
}))

const InvestSlider = withStyles({
  mark: {
    backgroundColor: '#bfbfbf',
    height: 8,
    width: 1,
    marginTop: -3,
  },
})(Slider)

export default () => {
  const classes = useStyles()
  const { locales, group, invested, investTemp, roundNum, initialEndowment, profit, requestState } = useStore()

  const { nowRound } = group

  const [open, setOpen] = useState(false)
  const [sliderValue, setSliderValue] = useState(Math.floor(initialEndowment / 2))
  const [wait, setWait] = useState(false)

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)
  const investMarks = [
    {
      value: 0,
    },
    {
      value: (Math.floor(initialEndowment / 10))
    },
    {
      value: (Math.floor(initialEndowment / 10 * 2))
    },
    {
      value: (Math.floor(initialEndowment / 10 * 3))
    },
    {
      value: (Math.floor(initialEndowment / 10 * 4))
    },
    {
      value: (Math.floor(initialEndowment / 10 * 5))
    },
    {
      value: (Math.floor(initialEndowment / 10 * 6))
    },
    {
      value: (Math.floor(initialEndowment / 10 * 7))
    },
    {
      value: (Math.floor(initialEndowment / 10 * 8))
    },
    {
      value: (Math.floor(initialEndowment / 10 * 9))
    },
    {
      value: initialEndowment
    },
  ]

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  const handleThinking = (_, value) => {
    setSliderValue(value)
  }

  const handleChanging = (_, value) => {
    setSliderValue(value)
  }

  const handleClick = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleSend = () => {
    setWait(true)
    requestState({ event: 'finish investing', payload: sliderValue, successCallback: callbackSend, timeoutCallback: callbackSend })
  }

  const callbackSend = () => {
    setWait(false)
    setOpen(false)
  }

  const handleInvestDialogKeyDown = event => {
    if (event.key === 'Enter' || event.keyCode === 13) {
      pushState({ event: 'finish investing', payload: sliderValue })
    }
  }

  const handleSliderKeyDown = event => {
    if (event.key === 'Enter' || event.keyCode === 13) {
      setOpen(true)
    }
  }

  return (
    <>
      {invested
        ? <Card className={classes.outer}>
          <CardContent>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Typography variant='body1' className={classes.profit}>{t('guest.experiment.profit_01', { profit: profit.toFixed(1) })}</Typography>
            </Grid>
            <Typography variant='body1'>{t('guest.experiment.investing.wait_other_01')}</Typography>
          </CardContent>
          <CardContent>
            <PieChart mode={'investing'} />
          </CardContent>
        </Card>
        : <Card className={classes.outer}>
          <CardContent>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Typography variant='body1' className={classes.profit}>{t('guest.experiment.profit_01', { profit: profit.toFixed(1) })}</Typography>
            </Grid>
            {roundNum === nowRound
              ? <Typography variant='body1'>{t('guest.experiment.investing.now_round_last_01', { now_round: nowRound })}</Typography>
              : <Typography variant='body1'>{t('guest.experiment.investing.now_round_01', { now_round: nowRound, left_round: roundNum - nowRound })}</Typography>
            }
            <Typography variant='body1'>{t('guest.experiment.investing.instruction_01')}</Typography>
            <div className={classes.sliderWrapper}>
              <InvestSlider
                min={0}
                max={Math.floor(initialEndowment)}
                step={1}
                marks={investMarks}
                value={sliderValue}
                valueLabelDisplay='on'
                onChange={handleChanging}
                onChangeCommitted={handleThinking}
                onKeyDown={handleSliderKeyDown}
              />
            </div>
            <InvestPieChart
              investGroup={sliderValue}
              investMe={initialEndowment - sliderValue}
            />
            <Dialog
              open={open}
              onClose={handleClose}
              onKeyDown={handleInvestDialogKeyDown}
            >
              <DialogContent className={classes.dialogContent}>
                <DialogContentText>
                  {t('guest.experiment.investing.confirm_01', { invest_group: sliderValue, invest_me: initialEndowment - sliderValue })}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Grid
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="flex-start"
                >
                  <Grid item xs={6} sm={4} className={classes.buttonGrid1}>
                    <Button variant='outlined' onClick={handleClose} color='secondary' className={classes.button} fullWidth>
                      {t('guest.experiment.investing.cancel_01')}
                    </Button>
                  </Grid>
                  <Grid item xs={6} sm={4} className={classes.buttonGrid2}>
                    <Button variant='contained' onClick={handleSend} color='primary' className={classes.button} fullWidth>
                      {t('guest.experiment.investing.invest_01')}
                    </Button>
                  </Grid>
                </Grid>
              </DialogActions>
            </Dialog>
          </CardContent>
          <CardActions>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="flex-start"
            >
              <Grid item xs={12} sm={4}>
                <Button
                  color='primary'
                  variant='contained'
                  onClick={handleClick}
                  className={classes.button}
                  fullWidth
                >
                  {wait && <CircularProgress size={24} className={classes.buttonProgress}/>}
                  {!wait && t('guest.experiment.investing.next_01')}
                </Button>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
      }
    </>
  )
}
