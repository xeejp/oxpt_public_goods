import React, { useReducer, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Switch from '@material-ui/core/Switch'

import IconButton from '@material-ui/core/IconButton'

import Add from '@material-ui/icons/Add'
import Remove from '@material-ui/icons/Remove'
import Settings from '@material-ui/icons/Settings'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  root: {
    padding: 0,
    height: '100vh',
    overflow: 'auto',
    margin: theme.spacing(1)
  },
  dialog: {
    height: '80vh',
  },
  dialogTitle: {
    margin: 0,
    padding: 0
  },
  formControl: {
    margin: theme.spacing(1),
  },
  group: {
    margin: theme.spacing(1, 0),
  },
}))

const TabPanel = (props) => {
  const { children, value, index, ...other } = props

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  )
}

const a11yProps = (index) => {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  }
}

const Setting = () => {
  const classes = useStyles()
  const { locales, page, requestState, roundNum, guestsPerGroup, isPunishment, maxPunishment, multiPunishment, payoffRate, initialEndowment } = useStore()

  if (!(locales))
    return <></>

  const [t, i18n] = useTranslation('translations', { i18nInstance })

  const [open, setOpen] = useState(page === 'instruction')

  const [, forceUpdate] = useReducer(x => x + 1, 0)
  const [tabValue, setTabValue] = useState(0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const [settingObject, setSettingObject] = useState({
    roundNumVar: roundNum,
    guestsPerGroupVar: guestsPerGroup,
    isPunishmentVar: isPunishment,
    maxPunishmentVar: maxPunishment,
    multiPunishmentVar: multiPunishment,
    payoffRateVar: payoffRate,
    initialEndowmentVar: initialEndowment
  })
  const [valid, setValid] = useState(true)

  const handleRoundNumDec = () => {
    if (settingObject.roundNumVar >= 2) setSettingObject(Object.assign({}, {...settingObject, roundNumVar: settingObject.roundNumVar - 1}))
  }

  const handleRoundNumInc = () => {
    setSettingObject(Object.assign({}, {...settingObject, roundNumVar: settingObject.roundNumVar + 1}))
  }

  const handleGuestsPerGroupDec = () => {
    if (settingObject.guestsPerGroupVar >= 2) setSettingObject(Object.assign({}, {...settingObject, guestsPerGroupVar: settingObject.guestsPerGroupVar - 1}))
  }

  const handleGuestsPerGroupInc = () => {
    setSettingObject(Object.assign({}, {...settingObject, guestsPerGroupVar: settingObject.guestsPerGroupVar + 1}))
  }

  const handleChangeIsPunishment = e => {
    const flag = !settingObject.isPunishmentVar
    settingObject["isPunishmentVar"] = flag
    setSettingObject(Object.assign({}, settingObject))
    onChangeSetValid(settingObject)
  }

  const handleChangeMaxPunishment = e => {
    const temp = e.target.value !== '' ? parseFloat(e.target.value) >= 0 ? parseFloat(e.target.value) : '' : ''
    settingObject["maxPunishmentVar"] = temp
    setSettingObject(Object.assign({}, settingObject))
    onChangeSetValid(settingObject)
  }

  const handleChangeMultiPunishment = e => {
    const temp = e.target.value !== '' ? parseFloat(e.target.value) : ''
    settingObject["multiPunishmentVar"] = temp
    setSettingObject(Object.assign({}, settingObject))
    onChangeSetValid(settingObject)
  }

  const handleChangePayOffRate = e => {
    const temp = e.target.value !== '' ? parseFloat(e.target.value) : ''
    settingObject["payoffRateVar"] = temp
    setSettingObject(Object.assign({}, settingObject))
    onChangeSetValid(settingObject)
  }

  const handleChangeInitialEndowment = e => {
    const temp = e.target.value !== '' ? parseFloat(e.target.value) >= 0 ? parseFloat(e.target.value) : '' : ''
    settingObject["initialEndowmentVar"] = temp
    setSettingObject(Object.assign({}, settingObject))
    onChangeSetValid(settingObject)
  }

  const onChangeSetValid = obj => {
    if (obj.isPunishmentVar) {
      if (obj.maxPunishmentVar === '' || obj.multiPunishmentVar === '' || obj.payoffRateVar === '' || obj.initialEndowmentVar === '') {
        setValid(false)
      } else {
        setValid(true)
      }
    } else {
      if (obj.payoffRateVar === '' || obj.initialEndowmentVar === '') {
        setValid(false)
      } else {
        setValid(true)
      }
    }
  }

  const handleOnCancel = () => {
    setOpen(false)
  }

  const handleOnSend = () => {
    requestState({
      event: 'setting',
      payload: {
        round_num: settingObject.roundNumVar,
        guests_per_group: settingObject.guestsPerGroupVar,
        is_punishment: settingObject.isPunishmentVar,
        max_punishment: settingObject.maxPunishmentVar,
        multi_punishment: settingObject.multiPunishmentVar,
        payoff_rate: settingObject.payoffRateVar,
        initial_endowment: settingObject.initialEndowmentVar,
        locales_temp: {}
      },
      successCallback: () => {},
      timeoutCallback: () => {}
    })
    setOpen(false)
  }

  const handleChangeTab = (event, newValue) => {
    setTabValue(newValue)
  }

  return (
    <div
      className={classes.root}
    >
      <Grid item xs={12}>
        <IconButton color="primary" aria-label={t('host.setting.title_01')} disabled={page !== 'instruction'} onClick={setOpen.bind(null, true)}>
          <Settings fontSize="large" />
        </IconButton>
        <Dialog onClose={setOpen.bind(null, false)} open={open} fullWidth={true} maxWidth="md">
          <DialogTitle>
            <AppBar position="relative" color="default">
              <Tabs
                value={tabValue}
                onChange={handleChangeTab}
                variant="scrollable"
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                aria-label="scrollable force tabs"
              >
                <Tab label={t('host.setting.game.title_01')} icon={<Settings />} {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </DialogTitle>
          <DialogContent>
            <TabPanel value={tabValue} index={0}>
              <TableContainer component={Paper}>
                <Table aria-label="setting table" size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell align="left">{t('host.setting.game.header_item_01')}</TableCell>
                      <TableCell align="center">{t('host.setting.game.header_info_01')}</TableCell>
                      <TableCell align="center">{t('host.setting.game.setting_01')}</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.round_num_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {settingObject.roundNumVar}
                      </TableCell>
                      <TableCell align="center">
                        <IconButton
                          color="primary"
                          aria-label="decrease round"
                          onClick={handleRoundNumDec}
                          disabled={settingObject.roundNumVar <= 1}
                        >
                          <Remove />
                        </IconButton>
                        <IconButton
                          color="secondary"
                          aria-label="increase round"
                          onClick={handleRoundNumInc}
                        >
                          <Add />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.guests_per_group_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {settingObject.guestsPerGroupVar}
                      </TableCell>
                      <TableCell align="center">
                        <IconButton
                          color="primary"
                          aria-label="decrease guests per group"
                          onClick={handleGuestsPerGroupDec}
                          disabled={settingObject.guestsPerGroupVar <= 1}
                        >
                          <Remove />
                        </IconButton>
                        <IconButton
                          color="secondary"
                          aria-label="increase guests per group"
                          onClick={handleGuestsPerGroupInc}
                        >
                          <Add />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.is_punishment_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {settingObject.isPunishmentVar ? t('host.setting.game.yes_punishment_01') : t('host.setting.game.no_punishment_01')}
                      </TableCell>
                      <TableCell align="center">
                        <Typography component="div">
                          <Grid component="label" container alignItems="center" justify="center" spacing={1}>
                            <Grid item>{t('host.setting.game.no_punishment_01')}</Grid>
                            <Grid item>
                              <Switch
                                checked={settingObject.isPunishmentVar}
                                onChange={handleChangeIsPunishment}
                                name="isPunishment"
                                color="primary"
                              />
                            </Grid>
                            <Grid item>{t('host.setting.game.yes_punishment_01')}</Grid>
                          </Grid>
                        </Typography>
                      </TableCell>
                    </TableRow>
                    {settingObject.isPunishmentVar
                      ? <TableRow>
                        <TableCell component="th" scope="row" align="left">
                          {t('host.setting.game.max_punishment_01')}
                        </TableCell>
                        <TableCell scope="row" align="right">
                          {settingObject.maxPunishmentVar}
                        </TableCell>
                        <TableCell align="center">
                          {settingObject.maxPunishmentVar !== ''
                            ? <TextField
                              type='number'
                              step='0.0001'
                              value={settingObject.maxPunishmentVar}
                              onChange={handleChangeMaxPunishment}
                            />
                            : <TextField
                              error
                              type='number'
                              step='0.0001'
                              helperText={t('host.setting.game.error_invalid_01')}
                              value={settingObject.maxPunishmentVar}
                              onChange={handleChangeMaxPunishment}
                            />
                          }
                        </TableCell>
                      </TableRow>
                      : null}
                    {settingObject.isPunishmentVar
                      ? <TableRow>
                        <TableCell component="th" scope="row" align="left">
                          {t('host.setting.game.multi_punishment_01')}
                        </TableCell>
                        <TableCell scope="row" align="right">
                          {settingObject.multiPunishmentVar}
                        </TableCell>
                        <TableCell align="center">
                          {settingObject.multiPunishmentVar !== ''
                            ? <TextField
                              type='number'
                              step='0.0001'
                              value={settingObject.multiPunishmentVar}
                              onChange={handleChangeMultiPunishment}
                            />
                            : <TextField
                              error
                              type='number'
                              step='0.0001'
                              helperText={t('host.setting.game.error_invalid_01')}
                              value={settingObject.multiPunishmentVar}
                              onChange={handleChangeMultiPunishment}
                            />
                          }
                        </TableCell>
                      </TableRow>
                      : null}
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.payoff_rate_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {settingObject.payoffRateVar}
                      </TableCell>
                      <TableCell align="center">
                        {settingObject.payoffRateVar !== ''
                          ? <TextField
                            type='number'
                            step='0.0001'
                            value={settingObject.payoffRateVar}
                            onChange={handleChangePayOffRate}
                          />
                          : <TextField
                            error
                            type='number'
                            step='0.0001'
                            helperText={t('host.setting.game.error_invalid_01')}
                            value={settingObject.payoffRateVar}
                            onChange={handleChangePayOffRate}
                          />
                        }
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell component="th" scope="row" align="left">
                        {t('host.setting.game.initial_endowment_01')}
                      </TableCell>
                      <TableCell scope="row" align="right">
                        {settingObject.initialEndowmentVar}
                      </TableCell>
                      <TableCell align="center">
                        {settingObject.initialEndowmentVar !== ''
                          ? <TextField
                            type='number'
                            step='0.0001'
                            value={settingObject.initialEndowmentVar}
                            onChange={handleChangeInitialEndowment}
                          />
                          : <TextField
                            error
                            type='number'
                            step='0.0001'
                            helperText={t('host.setting.game.error_invalid_01')}
                            value={settingObject.initialEndowmentVar}
                            onChange={handleChangeInitialEndowment}
                          />
                        }
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </TabPanel>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleOnCancel}>
              {t('host.setting.cancel_01')}
            </Button>
            <Button onClick={handleOnSend} color="primary" disabled={!valid }>
              {t('host.setting.send_01')}
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    </div>
  )
}

TabPanel.propTypes = {
  value: PropTypes.number,
  index: PropTypes.number
}

export default Setting
